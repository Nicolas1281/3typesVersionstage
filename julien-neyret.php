<?php
$menu = "none";
$title = "Julien Neyret - 3Types";
$bodyback = "#8386C0";
$project = "jn";
include "head.php";
?>
<script src="asset/js/shuffle.min.js"></script>
<script src="asset/js/gsap.min.js"></script>
<script src="asset/js/doublebutton.js"></script>

<img style="display:none;" src="asset/img/jn/image1.jpg" />
<img style="display:none;" src="asset/img/jn/image1.jpg" />
<div class="project-body" style="background-color: #8386C0;">
    <div class="project-first">
        <div class="project-first-inner project-first-desktop" style="background-image: url('asset/img/jn/image1.jpg');" border="151515">
            <img class="floating-arrow" id="arrow_scroll" src="asset/img/arrow_light.svg" />
        </div>
        <div class="project-first-inner project-first-mobile" style="background-image: url('asset/img/jn/image1.jpg');" border="151515">
            <img class="floating-arrow" id="arrow_scroll2" src="asset/img/arrow_light.svg" />
        </div>
    </div>

    <div class="project-info" id="project_info">
        <div class="project-client project-client-white">
            <h2>Client</h2>
            <hr>
            <p>
                Vidéaste et monteur freelance basé à Montpellier, Julien a sue se faire une place dans ce secteur avec sa créativité
                et son engagement. De l’écriture à la post-production, il sait mettre en valeur chaque histoire et projets.
                Julien a fait appel à nous afin de créer une identité visuelle et un site web à son image : Créative, sérieuse et jeune.
            </p>
        </div>
        <div class="project-keywords project-keywords-white">
            <span>06</span>
            <h2>Mots clés</h2>
            <hr>
            <div>
                <p>Logotype / Illustration / Direction artistique / Identité visuelle / Web design / Développement web </p>
                <strong>Vidéaste et Photographe</strong>
            </div>
        </div>
    </div>

    <div class="project-two-grid">
        <div class="jn-left">
            <img src="asset/img/jn/image2.svg" />
            <p>
                Identité Visuelle <br>
                Typographie <br>
                Web design <br>
                Web development
            </p>
            <span>
                Pour Julien Neyret <br>
                Vidéaste photographe <br>
                Montpellier
            </span>
        </div>
        <div class="jn-right">
            <img src="asset/img/jn/image3.svg" />
        </div>
    </div>

    <div class="jn-center">
        <img src="asset/img/jn/image4.svg" alt="">
        <img src="asset/img/jn/image5.svg" />
        <p>
            Dessin typographique personalisé
        </p>
    </div>

    <div class="project-two-grid">
        <div class="jn-left2">
            <img src="asset/img/jn/image6.svg" />
            <div>
                <p>
                    Un travailleur passionné
                </p>
                <p>
                    Julien est un vidéaste audassieux. Travailleur acharné, c’est sa passion pour l’image qui le guide depuis le premier jour.
                    Scénariste, vidéaste, photographe il mène à bien ses projets avec sérieux et bonne humeur.
                </p>
            </div>

            <img src="asset/img/jn/image7.svg" alt="">
        </div>
        <div class="jn-right2">
            <img src="asset/img/jn/image8.jpg" />
        </div>
    </div>

    <div class="footer-carousel">
        <img src="asset/img/jn/carouse1.svg" />
        <img src="asset/img/jn/carouse1.svg" />
        <img src="asset/img/jn/carouse1.svg" />
        <img src="asset/img/jn/carouse1.svg" />
        <img src="asset/img/jn/carouse1.svg" />
        <img src="asset/img/jn/carouse1.svg" />
        <img src="asset/img/jn/carouse1.svg" />
        <img src="asset/img/jn/carouse1.svg" />
    </div>


    <div class="jn-image-center">
        <img src="asset/img/jn/image9.svg" />
    </div>

    <div class="project-image-full mirage-video">
        <video autoplay muted loop playsinline width="100%">
            <source src="asset/video/jn/jn.webm" type="video/webm" />
            <source src="asset/video/jn/jn.mp4" type="video/mp4" />
            <source src="asset/video/jn/jn.mov" type="video/mov" />
        </video>
    </div>

    <div class="project-bottom" style="background-color: #3A3B67;">
        <div class="project-bottom-block">
            <h2>Julien Neyret</h2>
        </div>
        <div class="project-bottom-block">
            <h2>Vidéaste, Monteur, Cadreur</h2>
        </div>
        <div class="project-bottom-block">
            <h2>2022</h2>
        </div>
    </div>

    <div class="project-bottom-button" style="background-color: #0F0F0F;">
        <a href="projets" class="double-button studio-button-center">
            <div class="double-button-back">
                RETOUR&nbsp;AUX&nbsp;PROJETS
            </div>
            <p class="double-button-text">
                RETOUR&nbsp;AUX&nbsp;PROJETS
            </p>
        </a>
    </div>
</div>

<script src="asset/js/project.js"></script>
<?php
include "foot.php";
?>