<?php
$menu = "none";
$title = "ION-X - 3Types";
$bodyback = "#141C33";
$project = "ion-x";
include "head.php";
?>
<script src="asset/js/shuffle.min.js"></script>
<script src="asset/js/gsap.min.js"></script>
<script src="asset/js/doublebutton.js"></script>

<img style="display:none;" src="asset/img/ion-x/image1.jpg" />
<img style="display:none;" src="asset/img/ion-x/image1.jpg" />
<div class="project-body" style="background-color: #141C33;">
  <div class="project-first">
    <div class="project-first-inner project-first-desktop" style="background-image: url('asset/img/ion-x/image1.jpg');" border="151515">
      <img class="floating-arrow" id="arrow_scroll" src="asset/img/arrow_light.svg" />
    </div>
    <div class="project-first-inner project-first-mobile" style="background-image: url('asset/img/ion-x/image1.jpg');" border="151515">
      <img class="floating-arrow" id="arrow_scroll2" src="asset/img/arrow_light.svg" />
    </div>
  </div>

  <div class="project-info" id="project_info">
    <div class="project-client project-client-white">
      <h2>Client</h2>
      <hr>
      <p>
        ION-X est une startup Parisienne qui travaille dans le secteur du spatial. Elle s’emploie à créer une nouvelle technologie de propulseur spatial basé sur une
        technologie de propulsion d’ion à grande vitesse.
        ION-X a fait appel à nous pour créer son identité visuelle ainsi qu’un site web à son image.
      </p>
    </div>
    <div class="project-keywords project-keywords-white">
      <span>05</span>
      <h2>Mots clés</h2>
      <hr>
      <div>
        <p>Logotype / Direction artistique / Identité visuelle / Papèterie / Site Web </p>
        <strong>START-UP SPATIALE</strong>
      </div>
    </div>
  </div>

  <div class="project-two-grid">
    <div class="ionx-left">
      <img src="asset/img/ion-x/image3.svg" />
      <p>
        Identité Visuelle<br>
        Typographie<br>
        Web design<br>
        Web development<br>
        collaterals
      </p>
      <span>
        Website : <a href="https://ion-x.space" target="_blank">ion-x.space</a>
      </span>
    </div>
    <div class="ionx-right">
      <img src="asset/img/ion-x/image2.png" />
    </div>
  </div>

  <div class="ionx-center">
    <img src="asset/img/ion-x/image4.svg" alt="">
    <img src="asset/img/ion-x/image5.svg" alt="">
    <img src="asset/img/ion-x/image6.svg" alt="">
    <img src="asset/img/ion-x/image3.jpg" />
  </div>

  <div class="project-two-grid">
    <div class="ionx-left2">
      <img src="asset/img/ion-x/image8.svg" />
      <div>
        <p>
          FROM NANOSCIENCE <br>
          TO NEWSPACE
        </p>
        <p>
          The number of small satellites constellations in Low Earth Orbit is bound to skyrocket in the next decade to answer communication,
          earth observation, climate monitoring or military needs back on Earth.<br>
          Those satellites will require adequate propulsion systems to optimize their utilization and life-duration on orbit.
          ION-X develops and markets such propulsion systems: Simple, Efficient, and Powerful!
        </p>
      </div>
      <span>
        CNRS – C2N <br>
        10 boulevard Thomas Gobert <br>
        91120 Palaiseau <br>
        France
      </span>
    </div>
    <div class="ionx-right">
      <img src="asset/img/ion-x/image7.jpg" />
    </div>
  </div>

  <div class="project-image-large ionx-video">
    <video autoplay muted loop playsinline width="100%">
      <source src="asset/video/ion-x/ionx.webm" type="video/webm"/>
      <source src="asset/video/ion-x/ionx.mp4" type="video/mp4"/>
      <source src="asset/video/ion-x/ionx.mov" type="video/mov"/>
    </video>
  </div>

  <div class="project-bottom">
    <div class="project-bottom-block">
      <h2>ION-X</h2>
    </div>
    <div class="project-bottom-block">
      <h2>Propulseur de satelites</h2>
    </div>
    <div class="project-bottom-block">
      <h2>2022</h2>
    </div>
  </div>

  <div class="project-bottom-button" style="background-color: #0F0F0F;">
    <a href="projets" class="double-button studio-button-center">
      <div class="double-button-back">
        RETOUR&nbsp;AUX&nbsp;PROJETS
      </div>
      <p class="double-button-text">
        RETOUR&nbsp;AUX&nbsp;PROJETS
      </p>
    </a>
  </div>
</div>

<script src="asset/js/project.js"></script>
<?php
include "foot.php";
?>