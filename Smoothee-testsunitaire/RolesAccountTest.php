<?php

include_once INSTALL_ROOT . "/managers/RolesManager.php";
include_once INSTALL_ROOT . "/managers/AccountsManager.php";

class RolesAccountTest extends Test {
    public $name = "Test d'accès au compte";
    public $priority = 9;

    public function run() {
        $accountId = 1;

        // Saving the current role
        $sql = "SELECT * FROM `tests_" . DB_PREFIX . "accounts` WHERE id = ?";
        $account = getOneFromDatabase($sql, $accountId);

        if (!$account) {
            $this->setError("Le compte avec l'ID 1 n'a pas été trouvé");
            return;
        }

        $savePreviousRole = $account['role'];

        // parameters role
        $roleParams = [
            'name' => 'Dev',
            'accounts' => '0',
            'roles' => '2',
            'health' => '2',
            "tests" => '2',
            "websites" => "2",
            "items" => "2",
            "attributes" => "2"
        ];

        $roleId = $this->testApiRoute('roles/create', $roleParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$roleId) {
            return;
        }

        // Change role to 0 (lowest access level) via API
        $accountParams = [
            'id' => $accountId,
            'firstname' => 'Test',
            'lastname' => 'Agent',
            'login' => 'test',
            'email' => 'test@test.com',
            'role' => $roleId
        ];

        if (!$this->testApiRoute('accounts/edit', $accountParams)) {
            return;
        }

        // Role 0 tests accounts
        if (!$this->testPage(LOCAL_SERVER . INSTALL_DIR . '/accounts', 302)) {
            return;
        }

        // Role 0 tests create
        $login = "test_" . uniqid();
        $email = $login . '@gmail.com';

        $accountParams = [
            'id' => '',
            'firstname' => 'Jean',
            'lastname' => 'Dupont',
            'login' => $login,
            'email' => $email,
            "role" => $roleId,
            "password" => "password",
            "password_check" => "password"
        ];

        if ($this->testApiRoute('accounts/create', $accountParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/accounts/create fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests edit
        $accountParams = [
            'firstname' => 'Jean2',
            'lastname' => 'Dupont2',
            'login' => $login . "2",
            'email' => $email . "2",
            'role' => $roleId,
            'id' => ''
        ];

        if ($this->testApiRoute('accounts/edit', $accountParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/accounts/edit fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests screen
        $params = [
            'id' => '1',
            'screen' => 'editForm'
        ];
        if ($this->testApiRoute('accounts/screen', $params, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/accounts/screen fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests delete
        if ($this->testApiRoute('accounts/delete', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/accounts/delete fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests multiple_delete
        $deleteData = [
            'id' => [1, 2, 3]
        ];
        if ($this->testApiRoute('accounts/multiple_delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/accounts/multiple_delete fonctionne pour le rôle à 0");
            return;
        }

        // Edit the role 1 via API
        $roleParams['accounts'] = '1'; // Change the accounts parameter to '1' for editing
        $roleParams['id'] = $roleId;

        if (!$this->testApiRoute('roles/edit', $roleParams)) {
            return;
        }

        // Role 1 tests accounts
        if (!$this->testPage(LOCAL_SERVER . INSTALL_DIR . '/accounts', 200)) {
            return;
        }

        // Role 1 tests create
        $login = "test_" . uniqid();
        $email = $login . '@gmail.com';

        $accountParams = [
            'id' => '',
            'firstname' => 'Jean',
            'lastname' => 'Dupont',
            'login' => $login,
            'email' => $email,
            "role" => $roleId,
            "password" => "password",
            "password_check" => "password"
        ];

        if ($this->testApiRoute('accounts/create', $accountParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/accounts/create fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests edit
        $accountParams = [
            'firstname' => 'Jean2',
            'lastname' => 'Dupont2',
            'login' => $login . "2",
            'email' => $email . "2",
            'role' => $roleId,
            'id' => ''
        ];

        if ($this->testApiRoute('accounts/edit', $accountParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/accounts/edit fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests screen
        $params = [
            'id' => '1',
            'screen' => 'editForm'
        ];
        if (!$this->testApiRoute('accounts/screen', $params)) {
            return;
        }

        // Role 1 tests list
        $params = [
            'count' => '100',
            'page' => '0',
            'order' => 1
        ];
        if (!$this->testApiRoute('accounts/list', $params)) {
            return;
        }

        // Role 1 tests delete
        if ($this->testApiRoute('accounts/delete', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/accounts/delete fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests multiple_delete
        $deleteData = [
            'id' => [1, 2, 3]
        ];
        if ($this->testApiRoute('accounts/multiple_delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/accounts/multiple_delete fonctionne pour le rôle à 1");
            return;
        }

        // Edit the role 2 via API
        $roleParams['accounts'] = '2'; // Change the accounts parameter to '2' for editing
        $roleParams['id'] = $roleId;

        if (!$this->testApiRoute('roles/edit', $roleParams)) {
            return;
        }

        // Role 2 tests accounts
        if (!$this->testPage(LOCAL_SERVER . INSTALL_DIR . '/accounts', 200)) {
            return;
        }

        // Role 2 tests create
        $login = "test_" . uniqid();
        $email = $login . '@gmail.com';

        $accountParams = [
            'id' => '',
            'firstname' => 'Jean',
            'lastname' => 'Dupont',
            'login' => $login,
            'email' => $email,
            "role" => $roleId,
            "password" => "password",
            "password_check" => "password"
        ];

        $accountId = $this->testApiRoute('accounts/create', $accountParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$accountId) {
            return;
        }

        // Role 2 tests edit
        $accountParams = [
            'firstname' => 'Jean2',
            'lastname' => 'Dupont2',
            'login' => $login . "2",
            'email' => $email . "2",
            'role' => $roleId,
            'id' => $accountId
        ];

        if (!$this->testApiRoute('accounts/edit', $accountParams)) {
            return;
        }

        // Role 2 tests screen
        $params = [
            'id' => $accountId,
            'screen' => 'editForm'
        ];

        if (!$this->testApiRoute('accounts/screen', $params)) {
            return;
        }

        // Role 2 tests list
        $params = [
            'count' => '100',
            'page' => '0',
            'order' => 1
        ];

        if (!$this->testApiRoute('accounts/list', $params)) {
            return;
        }

        // Role 2 tests delete
        $deleteData = [
            'id' => $accountId
        ];

        if (!$this->testApiRoute('accounts/delete', $deleteData)) {
            return;
        }

        // Role 2 tests createMultipledelete1
        $login = "test_" . uniqid();
        $email = $login . '@gmail.com';

        $accountParams = [
            'firstname' => 'Jean',
            'lastname' => 'Dupont',
            'login' => $login,
            'email' => $email,
            "role" => $roleId,
            "password" => "password",
            "password_check" => "password"
        ];

        $accountIdOne = $this->testApiRoute('accounts/create', $accountParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$accountIdOne) {
            return;
        }

        // Role 2 tests createMultipledelete2
        $login = "test_" . uniqid();
        $email = $login . '@gmail.com';

        $accountParams = [
            'firstname' => 'Jean',
            'lastname' => 'Dupont',
            'login' => $login,
            'email' => $email,
            "role" => $roleId,
            "password" => "password",
            "password_check" => "password"
        ];

        $accountIdTwo = $this->testApiRoute('accounts/create', $accountParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$accountIdTwo) {
            return;
        }

        // Role 2 tests multiple_delete
        $deleteData = [
            'id' => "$accountIdOne,$accountIdTwo"
        ];

        if (!$this->testApiRoute('accounts/multiple_delete', $deleteData)) {
            return;
        }

        // Restoring the role via API
        $accountParams['role'] = $savePreviousRole;

        if (!$this->testApiRoute('accounts/edit', $accountParams)) {
            return;
        }

        // Delete the role created via the API
        $deleteData = [
            'id' => $roleId
        ];

        if (!$this->testApiRoute('roles/delete', $deleteData)) {
            return;
        }
    }
}
