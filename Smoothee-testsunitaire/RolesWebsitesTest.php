<?php

include_once INSTALL_ROOT . "/managers/RolesManager.php";
include_once INSTALL_ROOT . "/managers/AccountsManager.php";

class RolesWebsitesTest extends Test {
    public $name = "Test d'accès au site";
    public $priority = 6;

    public function run() {
        $accountId = 1;

        // Saving the current role
        $sql = "SELECT * FROM `tests_" . DB_PREFIX . "accounts` WHERE id = ?";
        $account = getOneFromDatabase($sql, $accountId);

        if (!$account) {
            $this->setError("Le compte avec l'ID 1 n'a pas été trouvé");
            return;
        }

        $savePreviousRole = $account['role'];

        // parameters role 0
        $roleParams = [
            'name' => 'Dev',
            'accounts' => '2',
            'roles' => '2',
            'health' => '2',
            "tests" => '2',
            "websites" => "0",
            "items" => "2",
            "attributes" => "2"
        ];

        $roleId = $this->testApiRoute('roles/create', $roleParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$roleId) {
            return;
        }

        // Change role to 0 (lowest access level) via API
        $accountParams = [
            'id' => $accountId,
            'firstname' => 'Test',
            'lastname' => 'Agent',
            'login' => 'test',
            'email' => 'test@test.com',
            'role' => $roleId
        ];

        if (!$this->testApiRoute('accounts/edit', $accountParams)) {
            return;
        }

        // Role 0 tests websites
        if (!$this->testPage(LOCAL_SERVER . INSTALL_DIR . '/websites', 302)) {
            return;
        }

        // Role 0 tests modal
        if ($this->testApiRoute('websites/modal', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/websites/modal fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests create
        $websiteParams = [
            'name' => 'teste',
            'api_key' => '',
        ];

        if ($this->testApiRoute('websites/create', $websiteParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/websites/create fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests edit
        $websiteParams['name'] = 'test2'; // Change the name parameter for editing

        if ($this->testApiRoute('websites/edit', $websiteParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/websites/edit fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests screen
        $params = [
            'id' => '1',
            'screen' => 'editForm'
        ];
        if ($this->testApiRoute('websites/screen', $params, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/websites/screen fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests delete
        if ($this->testApiRoute('websites/delete', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/websites/delete fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests multiple_delete
        $deleteData = [
            'id' => [1, 2, 3]
        ];
        if ($this->testApiRoute('websites/multiple_delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/websites/multiple_delete fonctionne pour le rôle à 0");
            return;
        }

        // Edit the role 1 via API
        $roleParams['websites'] = '1'; // Change the websites parameter to '1' for editing
        $roleParams['id'] = $roleId;

        if (!$this->testApiRoute('roles/edit', $roleParams)) {
            return;
        }

        // Role 1 tests websites
        if (!$this->testPage(LOCAL_SERVER . INSTALL_DIR . '/websites', 200)) {
            return;
        }

        // Role 1 tests modal
        if (!$this->testApiRoute('websites/modal', null)) {
            return;
        }

        // Role 1 tests create
        $websiteParams = [
            'name' => 'teste',
            'api_key' => '',
        ];

        if ($this->testApiRoute('websites/create', $websiteParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/websites/create fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests edit
        $websiteParams['name'] = 'test2'; // Change the name parameter for editing

        if ($this->testApiRoute('websites/edit', $websiteParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/websites/edit fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests screen
        $params = [
            'id' => '1',
            'screen' => 'editForm'
        ];
        if (!$this->testApiRoute('websites/screen', $params)) {
            return;
        }

        // Role 1 tests list
        $params = [
            'count' => '100',
            'page' => '0',
            'order' => 1
        ];
        if (!$this->testApiRoute('websites/list', $params)) {
            return;
        }

        // Role 1 tests delete
        if ($this->testApiRoute('websites/delete', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/websites/delete fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests multiple_delete
        $deleteData = [
            'id' => [1, 2, 3]
        ];
        if ($this->testApiRoute('websites/multiple_delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/websites/multiple_delete fonctionne pour le rôle à 1");
            return;
        }

        // Edit the role 2 via API
        $roleParams['websites'] = '2'; // Change the websites parameter to '2' for editing
        $roleParams['id'] = $roleId;

        if (!$this->testApiRoute('roles/edit', $roleParams)) {
            return;
        }

        // Role 2 tests websites
        if (!$this->testPage(LOCAL_SERVER . INSTALL_DIR . '/websites', 200)) {
            return;
        }

        // Role 2 tests modal
        if (!$this->testApiRoute('websites/modal', null)) {
            return;
        }

        // Role 2 tests create
        $websiteParams = [
            'name' => 'teste',
            'api_key' => '',
        ];

        $websiteId = $this->testApiRoute('websites/create', $websiteParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$websiteId) {
            return;
        }

        // Role 2 tests edit
        $websiteParams['name'] = 'test2'; // Change the name parameter for editing
        $websiteParams['id'] = $websiteId;

        if (!$this->testApiRoute('websites/edit', $websiteParams)) {
            return;
        }

        // Role 2 tests screen
        $params = [
            'id' => $websiteId,
            'screen' => 'editForm'
        ];

        if (!$this->testApiRoute('websites/screen', $params)) {
            return;
        }

        // Role 2 tests list
        $params = [
            'count' => '100',
            'page' => '0',
            'order' => 1
        ];

        if (!$this->testApiRoute('websites/list', $params)) {
            return;
        }

        // Role 2 tests delete
        $deleteData = [
            'id' => $websiteId
        ];

        if (!$this->testApiRoute('websites/delete', $deleteData)) {
            return;
        }

        // Role 2 tests createMultipledelete1
        $websiteParams = [
            'name' => 'teste1',
            'api_key' => '',
        ];

        $websiteIdOne = $this->testApiRoute('websites/create', $websiteParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$websiteIdOne) {
            return;
        }

        // Role 2 tests createMultipledelete2
        $websiteParams = [
            'name' => 'teste2',
            'api_key' => '',
        ];

        $websiteIdTwo = $this->testApiRoute('websites/create', $websiteParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$websiteIdTwo) {
            return;
        }

        // Role 2 tests multiple_delete
        $deleteData = [
            'id' => "$websiteIdOne,$websiteIdTwo"
        ];

        if (!$this->testApiRoute('websites/multiple_delete', $deleteData)) {
            return;
        }

        // Restoring the role via API
        $accountParams['role'] = $savePreviousRole;

        if (!$this->testApiRoute('accounts/edit', $accountParams)) {
            return;
        }

        // Delete the role created via the API
        $deleteData = [
            'id' => $roleId
        ];

        if (!$this->testApiRoute('roles/delete', $deleteData)) {
            return;
        }
    }
}
