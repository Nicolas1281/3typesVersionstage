<?php

include_once INSTALL_ROOT . "/managers/RolesManager.php";
include_once INSTALL_ROOT . "/managers/AccountsManager.php";

class RolesAttributesTest extends Test {
    public $name = "Test d'accès au attribut";
    public $priority = 8;

    public function run() {
        $accountId = 1;

        // Saving the current role
        $sql = "SELECT * FROM `tests_" . DB_PREFIX . "accounts` WHERE id = ?";
        $account = getOneFromDatabase($sql, $accountId);

        if (!$account) {
            $this->setError("Le compte avec l'ID 1 n'a pas été trouvé");
            return;
        }

        $savePreviousRole = $account['role'];

        // parameters role 0
        $roleParams = [
            'name' => 'Dev',
            'accounts' => '2',
            'roles' => '2',
            'health' => '2',
            "tests" => '2',
            "websites" => "2",
            "items" => "2",
            "attributes" => "0"
        ];

        $roleId = $this->testApiRoute('roles/create', $roleParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$roleId) {
            return;
        }

        // Change role to 0 (lowest access level) via API
        $accountParams = [
            'id' => $accountId,
            'firstname' => 'Test',
            'lastname' => 'Agent',
            'login' => 'test',
            'email' => 'test@test.com',
            'role' => $roleId
        ];

        if (!$this->testApiRoute('accounts/edit', $accountParams)) {
            return;
        }

        // CREATE WEBSITE PARENT
        // Create a website
        $websiteParams = [
            'name' => 'teste',
            'api_key' => '',
        ];

        $websiteId = $this->testApiRoute('websites/create', $websiteParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$websiteId) {
            return;
        }

        // CREATE ITEM PARENT
        $itemParams = [
            'name' => 'Test',
            'parent_id' => $websiteId,
            'plural' => 'Tests',
            'techName' => 'test',
            'techPlural' => 'tests',
            'icon' => 'face',
            'descriptionColumn' => '3',
            'iconColumn' => '0',
            'defaultOrderColumn' => '3',
            'defaultOrderDirection' => '0',
            'gender' => '1',
            'deactivable' => '1',
            'sortable' => '1',
            'is_child' => '1',
            'parent' => '93',
        ];

        $itemId = $this->testApiRoute('items/create', $itemParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$itemId) {
            return;
        }

        // Role 0 tests create
        $attributeParams = [
            'num' => '0',
            'name' => 'Test',
            'parent_id' => $itemId,
            'techName' => 'test',
            'icon' => 'face',
            'type' => '',
            'orderable' => '1',
            'searchable' => '1',
            'is_unique' => '1',
            'nullable' => '1',
            'help' => '',
            'default_value' => '',
            'default_checked' => '1',
            'default_file' => '',
            'length' => '',
            'autocomplete' => '1',
            'strict' => '1',
            'is_float' => '1',
            'min' => '',
            'max' => '',
            'nb_integers' => '',
            'nb_decimals' => '',
            'step' => '',
            'use_box' => '1',
            'false_text' => '',
            'true_text' => '',
            'images' => '1',
            'embeds' => '1',
            'titles' => '1',
            'font_sizes' => '1',
            'reorder' => '1',
            'max_size' => '',
            'max_height' => '',
            'max_width' => '',
            'comp_jpg' => '1',
            'comp_webp' => '1',
            'interlace' => '1',
            'comp_png' => '1',
            'mime_types' => '',
            'rows_managment' => '1',
            'multiple' => '1',
            'is_range' => '1',
            'chips' => '1',
            'search' => '1',
            'values_source' => '',
            'values_table' => '',
            'values_file' => '',
            'values_text_attr' => '',
            'values_value_attr' => '',
            'values_icon_attr' => '',
            'values_disabled_attr' => '',
            'values_text' => '',
            'values_value' => '',
            'values_icon' => '',
            'values_disabled' => '',
            'values_use_values' => '1',
            'values_use_icons' => '1',
            'values_use_disabled' => '1',
            'csv_separator' => '',
            'select_type' => '',
            'reference' => '92',
            'values_reference' => '',
            'values_ref_text_attr' => '',
            'values_ref_value_attr' => '',
            'values_ref_icon_attr' => '',
            'values_ref_disabled_attr' => '3',
            'password_strength' => '1',
            'password_check' => '1',
        ];

        if ($this->testApiRoute('attributes/create', $attributeParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/attributes/create fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests edit
        $attributeParams['name'] = 'test2'; // Change the name parameter for editing

        if ($this->testApiRoute('attributes/edit', $attributeParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/attributes/edit fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests screen
        $params = [
            'id' => '1',
            'screen' => 'editForm'
        ];

        if ($this->testApiRoute('attributes/screen', $params, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/attributes/screen fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests delete
        if ($this->testApiRoute('attributes/delete', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/attributes/delete fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests multiple_delete
        $deleteData = [
            'id' => [1, 2, 3]
        ];
        if ($this->testApiRoute('attributes/multiple_delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/attributes/multiple_delete fonctionne pour le rôle à 0");
            return;
        }

        // Edit the role 1 via API
        $roleParams['attributes'] = '1'; // Change the attributes parameter to '1' for editing
        $roleParams['id'] = $roleId;

        if (!$this->testApiRoute('roles/edit', $roleParams)) {
            return;
        }

        // Role 1 tests create
        if ($this->testApiRoute('attributes/create', $attributeParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/attributes/create fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests edit
        $attributeParams['name'] = 'test2'; // Change the name parameter for editing

        if ($this->testApiRoute('attributes/edit', $attributeParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/attributes/edit fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests screen
        $params = [
            'id' => '1',
            'screen' => 'editForm'
        ];

        if (!$this->testApiRoute('attributes/screen', $params)) {
            return;
        }

        // Role 1 tests list
        $params = [
            'count' => '100',
            'page' => '0',
            'order' => 1
        ];
        if (!$this->testApiRoute('attributes/list', $params)) {
            return;
        }

        // Role 1 tests delete
        if ($this->testApiRoute('attributes/delete', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/attributes/delete fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests multiple_delete
        $deleteData = [
            'id' => [1, 2, 3]
        ];
        if ($this->testApiRoute('attributes/multiple_delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/attributes/multiple_delete fonctionne pour le rôle à 1");
            return;
        }

        // Edit the role 2 via API
        $roleParams['attributes'] = '2'; // Change the attributes parameter to '2' for editing
        $roleParams['id'] = $roleId;

        if (!$this->testApiRoute('roles/edit', $roleParams)) {
            return;
        }

        // Role 2 tests create
        $attributeParams = [
            'num' => '0',
            'name' => 'Test',
            'parent_id' => $itemId,
            'techName' => 'test',
            'icon' => 'face',
            'type' => '',
            'orderable' => '1',
            'searchable' => '1',
            'is_unique' => '1',
            'nullable' => '1',
            'help' => '',
            'default_value' => '',
            'default_checked' => '1',
            'default_file' => '',
            'length' => '',
            'autocomplete' => '1',
            'strict' => '1',
            'is_float' => '1',
            'min' => '',
            'max' => '',
            'nb_integers' => '',
            'nb_decimals' => '',
            'step' => '',
            'use_box' => '1',
            'false_text' => '',
            'true_text' => '',
            'images' => '1',
            'embeds' => '1',
            'titles' => '1',
            'font_sizes' => '1',
            'reorder' => '1',
            'max_size' => '',
            'max_height' => '',
            'max_width' => '',
            'comp_jpg' => '1',
            'comp_webp' => '1',
            'interlace' => '1',
            'comp_png' => '1',
            'mime_types' => '',
            'rows_managment' => '1',
            'multiple' => '1',
            'is_range' => '1',
            'chips' => '1',
            'search' => '1',
            'values_source' => '',
            'values_table' => '',
            'values_file' => '',
            'values_text_attr' => '',
            'values_value_attr' => '',
            'values_icon_attr' => '',
            'values_disabled_attr' => '',
            'values_text' => '',
            'values_value' => '',
            'values_icon' => '',
            'values_disabled' => '',
            'values_use_values' => '1',
            'values_use_icons' => '1',
            'values_use_disabled' => '1',
            'csv_separator' => '',
            'select_type' => '',
            'reference' => '92',
            'values_reference' => '',
            'values_ref_text_attr' => '',
            'values_ref_value_attr' => '',
            'values_ref_icon_attr' => '',
            'values_ref_disabled_attr' => '3',
            'password_strength' => '1',
            'password_check' => '1',
        ];

        $attributId = $this->testApiRoute('attributes/create', $attributeParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$attributId) {
            return;
        }

        // Role 2 tests edit
        $attributeParams = [
            'num' => '0',
            'name' => 'Test2',
            'parent_id' => $itemId,
            'techName' => 'test',
            'icon' => 'face',
            'type' => '',
            'orderable' => '1',
            'searchable' => '1',
            'is_unique' => '1',
            'nullable' => '1',
            'help' => '',
            'default_value' => '',
            'default_checked' => '1',
            'default_file' => '',
            'length' => '',
            'autocomplete' => '1',
            'strict' => '1',
            'is_float' => '1',
            'min' => '',
            'max' => '',
            'nb_integers' => '',
            'nb_decimals' => '',
            'step' => '',
            'use_box' => '1',
            'false_text' => '',
            'true_text' => '',
            'images' => '1',
            'embeds' => '1',
            'titles' => '1',
            'font_sizes' => '1',
            'reorder' => '1',
            'max_size' => '',
            'max_height' => '',
            'max_width' => '',
            'comp_jpg' => '1',
            'comp_webp' => '1',
            'interlace' => '1',
            'comp_png' => '1',
            'mime_types' => '',
            'rows_managment' => '1',
            'multiple' => '1',
            'is_range' => '1',
            'chips' => '1',
            'search' => '1',
            'values_source' => '',
            'values_table' => '',
            'values_file' => '',
            'values_text_attr' => '',
            'values_value_attr' => '',
            'values_icon_attr' => '',
            'values_disabled_attr' => '',
            'values_text' => '',
            'values_value' => '',
            'values_icon' => '',
            'values_disabled' => '',
            'values_use_values' => '1',
            'values_use_icons' => '1',
            'values_use_disabled' => '1',
            'csv_separator' => '',
            'select_type' => '',
            'reference' => '92',
            'values_reference' => '',
            'values_ref_text_attr' => '',
            'values_ref_value_attr' => '',
            'values_ref_icon_attr' => '',
            'values_ref_disabled_attr' => '3',
            'password_strength' => '1',
            'password_check' => '1',
            'id' => $attributId
        ];

        if (!$this->testApiRoute('attributes/edit', $attributeParams)) {
            return;
        }

        // Role 2 tests screen
        $params = [
            'id' => $attributId,
            'screen' => 'editForm'
        ];

        if (!$this->testApiRoute('attributes/screen', $params)) {
            return;
        }

        // Role 2 tests list
        $params = [
            'count' => '100',
            'page' => '0',
            'order' => 1
        ];

        if (!$this->testApiRoute('attributes/list', $params)) {
            return;
        }

        // Role 2 tests delete
        $deleteData = [
            'id' => $attributId
        ];

        if (!$this->testApiRoute('attributes/delete', $deleteData)) {
            return;
        }

        // Role 2 tests createMultipledelete1
        $attributeParams = [
            'num' => '0',
            'name' => 'Test',
            'parent_id' => $itemId,
            'techName' => 'test',
            'icon' => 'face',
            'type' => '',
            'orderable' => '1',
            'searchable' => '1',
            'is_unique' => '1',
            'nullable' => '1',
            'help' => '',
            'default_value' => '',
            'default_checked' => '1',
            'default_file' => '',
            'length' => '',
            'autocomplete' => '1',
            'strict' => '1',
            'is_float' => '1',
            'min' => '',
            'max' => '',
            'nb_integers' => '',
            'nb_decimals' => '',
            'step' => '',
            'use_box' => '1',
            'false_text' => '',
            'true_text' => '',
            'images' => '1',
            'embeds' => '1',
            'titles' => '1',
            'font_sizes' => '1',
            'reorder' => '1',
            'max_size' => '',
            'max_height' => '',
            'max_width' => '',
            'comp_jpg' => '1',
            'comp_webp' => '1',
            'interlace' => '1',
            'comp_png' => '1',
            'mime_types' => '',
            'rows_managment' => '1',
            'multiple' => '1',
            'is_range' => '1',
            'chips' => '1',
            'search' => '1',
            'values_source' => '',
            'values_table' => '',
            'values_file' => '',
            'values_text_attr' => '',
            'values_value_attr' => '',
            'values_icon_attr' => '',
            'values_disabled_attr' => '',
            'values_text' => '',
            'values_value' => '',
            'values_icon' => '',
            'values_disabled' => '',
            'values_use_values' => '1',
            'values_use_icons' => '1',
            'values_use_disabled' => '1',
            'csv_separator' => '',
            'select_type' => '',
            'reference' => '92',
            'values_reference' => '',
            'values_ref_text_attr' => '',
            'values_ref_value_attr' => '',
            'values_ref_icon_attr' => '',
            'values_ref_disabled_attr' => '3',
            'password_strength' => '1',
            'password_check' => '1',
        ];

        $attributIdOne = $this->testApiRoute('attributes/create', $attributeParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$attributIdOne) {
            return;
        }

        // Role 2 tests createMultipledelete2
        $attributeParams = [
            'num' => '0',
            'name' => 'Test',
            'parent_id' => $itemId,
            'techName' => 'test',
            'icon' => 'face',
            'type' => '',
            'orderable' => '1',
            'searchable' => '1',
            'is_unique' => '1',
            'nullable' => '1',
            'help' => '',
            'default_value' => '',
            'default_checked' => '1',
            'default_file' => '',
            'length' => '',
            'autocomplete' => '1',
            'strict' => '1',
            'is_float' => '1',
            'min' => '',
            'max' => '',
            'nb_integers' => '',
            'nb_decimals' => '',
            'step' => '',
            'use_box' => '1',
            'false_text' => '',
            'true_text' => '',
            'images' => '1',
            'embeds' => '1',
            'titles' => '1',
            'font_sizes' => '1',
            'reorder' => '1',
            'max_size' => '',
            'max_height' => '',
            'max_width' => '',
            'comp_jpg' => '1',
            'comp_webp' => '1',
            'interlace' => '1',
            'comp_png' => '1',
            'mime_types' => '',
            'rows_managment' => '1',
            'multiple' => '1',
            'is_range' => '1',
            'chips' => '1',
            'search' => '1',
            'values_source' => '',
            'values_table' => '',
            'values_file' => '',
            'values_text_attr' => '',
            'values_value_attr' => '',
            'values_icon_attr' => '',
            'values_disabled_attr' => '',
            'values_text' => '',
            'values_value' => '',
            'values_icon' => '',
            'values_disabled' => '',
            'values_use_values' => '1',
            'values_use_icons' => '1',
            'values_use_disabled' => '1',
            'csv_separator' => '',
            'select_type' => '',
            'reference' => '92',
            'values_reference' => '',
            'values_ref_text_attr' => '',
            'values_ref_value_attr' => '',
            'values_ref_icon_attr' => '',
            'values_ref_disabled_attr' => '3',
            'password_strength' => '1',
            'password_check' => '1',
        ];

        $attributIdTwo = $this->testApiRoute('attributes/create', $attributeParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$attributIdTwo) {
            return;
        }

        // Role 2 tests multiple_delete
        $deleteData = [
            'id' => "$attributIdOne,$attributIdTwo"
        ];

        if (!$this->testApiRoute('attributes/multiple_delete', $deleteData)) {
            return;
        }

        // DELETE ITEM PARENT
        // Delete the item created via the API
        $deleteData = [
            'id' => $itemId
        ];

        if (!$this->testApiRoute('items/delete', $deleteData)) {
            return;
        }

        // DELETE WEBSITE PARENT
        // Delete the website created via the API
        $deleteData = [
            'id' => $websiteId
        ];

        if (!$this->testApiRoute('websites/delete', $deleteData)) {
            return;
        }

        // Restoring the role via API
        $accountParams['role'] = $savePreviousRole;

        if (!$this->testApiRoute('accounts/edit', $accountParams)) {
            return;
        }

        // Delete the role created via the API
        $deleteData = [
            'id' => $roleId
        ];

        if (!$this->testApiRoute('roles/delete', $deleteData)) {
            return;
        }
    }
}
