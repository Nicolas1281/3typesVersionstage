<?php

include_once INSTALL_ROOT . "/managers/RolesManager.php";
include_once INSTALL_ROOT . "/managers/AccountsManager.php";

class RolesAccesTest extends Test {
    public $name = "Test d'accès au role";
    public $priority = 10;

    public function run() {
        $accountId = 1;

        // Saving the current role
        $sql = "SELECT * FROM `tests_" . DB_PREFIX . "accounts` WHERE id = ?";
        $account = getOneFromDatabase($sql, $accountId);

        if (!$account) {
            $this->setError("Le compte avec l'ID 1 n'a pas été trouvé");
            return;
        }

        $savePreviousRole = $account['role'];

        // parameters role 0
        $roleParams = [
            'name' => 'Dev',
            'accounts' => '2',
            'roles' => '0',
            'health' => '2',
            "tests" => '2',
            "websites" => "2",
            "items" => "2",
            "attributes" => "2"
        ];

        $roleId = $this->testApiRoute('roles/create', $roleParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$roleId) {
            return;
        }

        // Change role to 0 (lowest access level) via API
        $accountParams = [
            'id' => $accountId,
            'firstname' => 'Test',
            'lastname' => 'Agent',
            'login' => 'test',
            'email' => 'test@test.com',
            'role' => $roleId
        ];

        if (!$this->testApiRoute('accounts/edit', $accountParams)) {
            return;
        }

        // Role 0 tests roles
        if (!$this->testPage(LOCAL_SERVER . INSTALL_DIR . '/roles', 302)) {
            return;
        }

        // Role 0 tests modal
        if ($this->testApiRoute('roles/modal', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/roles/modal fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests create
        $roleParams = [
            'name' => 'Dev',
            'accounts' => '2',
            'roles' => '2',
            'health' => '2',
            "tests" => '2',
            "websites" => "2",
            "items" => "2",
            "attributes" => "2"
        ];

        if ($this->testApiRoute('roles/create', $roleParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/roles/create fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests edit
        $roleParams = [
            'name' => 'Dev2',
            'accounts' => '1',
            'roles' => '1',
            'health' => '1',
            "tests" => '1',
            "websites" => "1",
            "items" => "1",
            "attributes" => "1",
        ];

        if ($this->testApiRoute('roles/edit', $roleParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/roles/edit fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests screen
        $params = [
            'id' => '1',
            'screen' => 'editForm'
        ];
        if ($this->testApiRoute('roles/screen', $params, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/roles/screen fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests delete
        if ($this->testApiRoute('roles/delete', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/roles/delete fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests multiple_delete
        $deleteData = [
            'id' => [1, 2, 3]
        ];
        if ($this->testApiRoute('roles/multiple_delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/roles/multiple_delete fonctionne pour le rôle à 0");
            return;
        }

        // Edit the role 1 via API
        $roleParams['roles'] = '1'; // Change the roles parameter to '1' for editing
        $roleParams['id'] = $roleId;

        if (!$this->testApiRoute('roles/edit', $roleParams)) {
            return;
        }

        // Role 1 tests roles
        if (!$this->testPage(LOCAL_SERVER . INSTALL_DIR . '/roles', 200)) {
            return;
        }

        // Role 1 tests modal
        if (!$this->testApiRoute('roles/modal', null)) {
            return;
        }

        // Role 1 tests create
        $roleParams = [
            'name' => 'Dev',
            'accounts' => '2',
            'roles' => '2',
            'health' => '2',
            "tests" => '2',
            "websites" => "2",
            "items" => "2",
            "attributes" => "2"
        ];

        if ($this->testApiRoute('roles/create', $roleParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/roles/create fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests edit
        $roleParams = [
            'name' => 'Dev2',
            'accounts' => '1',
            'roles' => '1',
            'health' => '1',
            "tests" => '1',
            "websites" => "1",
            "items" => "1",
            "attributes" => "1",
        ];

        if ($this->testApiRoute('roles/edit', $roleParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/roles/edit fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests screen
        $params = [
            'id' => '1',
            'screen' => 'editForm'
        ];
        if (!$this->testApiRoute('roles/screen', $params)) {
            return;
        }

        // Role 1 tests list
        $params = [
            'count' => '100',
            'page' => '0',
            'order' => 1
        ];
        if (!$this->testApiRoute('roles/list', $params)) {
            return;
        }

        // Role 1 tests delete
        if ($this->testApiRoute('roles/delete', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/roles/delete fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests multiple_delete
        $deleteData = [
            'id' => [1, 2, 3]
        ];
        if ($this->testApiRoute('roles/multiple_delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/roles/multiple_delete fonctionne pour le rôle à 1");
            return;
        }

        // Edit the role 2 via API
        $roleParams['roles'] = '2'; // Change the roles parameter to '2' for editing
        $roleParams['id'] = $roleId;

        if (!$this->testApiRoute('roles/edit', $roleParams)) {
            return;
        }

        // Role 2 tests roles
        if (!$this->testPage(LOCAL_SERVER . INSTALL_DIR . '/roles', 200)) {
            return;
        }

        // Role 2 tests modal
        if (!$this->testApiRoute('roles/modal', null)) {
            return;
        }

        // Role 2 tests create
        $roleParams = [
            'name' => 'Dev',
            'accounts' => '2',
            'roles' => '2',
            'health' => '2',
            "tests" => '2',
            "websites" => "2",
            "items" => "2",
            "attributes" => "2"
        ];

        $roleId = $this->testApiRoute('roles/create', $roleParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$roleId) {
            return;
        }

        // Role 2 tests edit
        $roleParams = [
            'name' => 'Dev2',
            'accounts' => '1',
            'roles' => '1',
            'health' => '1',
            "tests" => '1',
            "websites" => "1",
            "items" => "1",
            "attributes" => "1",
            'id' => $roleId
        ];
        if (!$this->testApiRoute('roles/edit', $roleParams)) {
            return;
        }

        // Role 2 tests screen
        $params = [
            'id' => $roleId,
            'screen' => 'editForm'
        ];

        if (!$this->testApiRoute('roles/screen', $params)) {
            return;
        }

        // Role 2 tests list
        $params = [
            'count' => '100',
            'page' => '0',
            'order' => 1
        ];

        if (!$this->testApiRoute('roles/list', $params)) {
            return;
        }

        // Role 2 tests delete
        $deleteData = [
            'id' => $roleId
        ];

        if (!$this->testApiRoute('roles/delete', $deleteData)) {
            return;
        }

        // Role 2 tests createMultipledelete1
        $roleParams = [
            'name' => 'Dev',
            'accounts' => '2',
            'roles' => '2',
            'health' => '2',
            "tests" => '2',
            "websites" => "2",
            "items" => "2",
            "attributes" => "2"
        ];

        $roleIdOne = $this->testApiRoute('roles/create', $roleParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$roleIdOne) {
            return;
        }

        // Role 2 tests createMultipledelete2
        $roleParams = [
            'name' => 'Dev',
            'accounts' => '2',
            'roles' => '2',
            'health' => '2',
            "tests" => '2',
            "websites" => "2",
            "items" => "2",
            "attributes" => "2"
        ];

        $roleIdTwo = $this->testApiRoute('roles/create', $roleParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$roleIdTwo) {
            return;
        }

        // Role 2 tests multiple_delete
        $deleteData = [
            'id' => "$roleIdOne,$roleIdTwo"
        ];

        if (!$this->testApiRoute('roles/multiple_delete', $deleteData)) {
            return;
        }

        // Restoring the role via API
        $accountParams['role'] = $savePreviousRole;

        if (!$this->testApiRoute('accounts/edit', $accountParams)) {
            return;
        }

        // Delete the role created via the API
        $deleteData = [
            'id' => $roleId
        ];

        if (!$this->testApiRoute('roles/delete', $deleteData)) {
            return;
        }
    }
}
