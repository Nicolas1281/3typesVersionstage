<?php

include_once INSTALL_ROOT . "/managers/RolesManager.php";

class RolesTest extends Test {
    public $name = "Test Roles";
    public $priority = 2;

    public function run() {

        // Test parameters role
        $roleParams = [
            'name' => 'Dev',
            'accounts' => '2',
            'roles' => '2',
            'health' => '2',
            "tests" => '2',
            "websites" => "2",
            "items" => "2",
            "attributes" => "2"
        ];

        // Witness
        $roleWitness = new Role($roleParams);
        if ($roleWitness == null) {
            $this->setError("Erreur lors de la création du rôle témoin.");
            return;
        }

        // CREATE SUCCESS
        // Create a role via API
        $roleId = $this->testApiRoute('roles/create', $roleParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$roleId) {
            return;
        }

        // Check if the role exists in the database
        $sql = "SELECT * FROM `tests_" . Role::TABLE . "` WHERE id = ?";
        $result = getOneFromDatabase($sql, $roleId);
        if (is_a($result, "OroshiError")) {
            $this->setError("Erreur SQL lors de la récupération du rôle:<br>$result->message");
            return;
        }
        // Check role attributes
        unset($result["id"]);
        $role = new Role($result);
        if ($this->compareObjects($roleWitness, $role)) {
            return;
        }

        // EDIT SUCCESS

        // Update the witness role
        $roleWitness->name = "Dev2";
        $roleWitness->accounts = "1";
        $roleWitness->roles = "1";
        $roleWitness->health = "1";
        $roleWitness->tests = "1";
        $roleWitness->websites = "1";
        $roleWitness->items = "1";
        $roleWitness->attributes = "1";

        // Edit the role via API
        $roleParams = [
            'name' => 'Dev2',
            'accounts' => '1',
            'roles' => '1',
            'health' => '1',
            "tests" => '1',
            "websites" => "1",
            "items" => "1",
            "attributes" => "1",
            'id' => $roleId
        ];
        if (!$this->testApiRoute('roles/edit', $roleParams)) {
            return;
        }

        // Check if the role exists in the database after editing
        $sql = "SELECT * FROM `tests_" . Role::TABLE . "` WHERE id = ?";
        $result = getOneFromDatabase($sql, $roleId);
        if (is_a($result, "OroshiError")) {
            $this->setError("Erreur SQL lors de la récupération du rôle après l'édition:<br>$result->message");
            return;
        }
        // Check role attributes after editing
        unset($result["id"]);
        $role = new Role($result);
        if ($this->compareObjects($roleWitness, $role)) {
            return;
        }

        // EDIT ERROR
        // Attempt to edit the role with an invalid request via API
        if ($this->testApiRoute('roles/edit', ['id' => $roleId], API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("Une erreur était attendue lors de l'édition d'un rôle invalide");
            return;
        }

        // CREATE ERROR
        // Attempt to create an invalid role via API
        if ($this->testApiRoute('roles/create', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("Une erreur était attendue lors de la création d'un rôle invalide");
            return;
        }

        // DELETE SUCCESS
        // Delete the role created via the API
        $deleteData = [
            'id' => $roleId
        ];

        if (!$this->testApiRoute('roles/delete', $deleteData)) {
            return;
        }

        // DELETE ERROR
        // Try to delete the role again
        if ($this->testApiRoute('roles/delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("On aurait du avoir une erreur lors de la 2éme suppression du rôle");
            return;
        }
    }
}
