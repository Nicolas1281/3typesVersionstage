<?php

include_once INSTALL_ROOT . "/managers/AttributesManager.php";

class AttributesTest extends Test {
    public $name = "Test Attribut";
    public $priority = 5;

    public function run() {

        // CREATE WEBSITE PARENT
        // Create a website
        $websiteParams = [
            'name' => 'teste',
            'api_key' => '',
        ];

        $websiteId = $this->testApiRoute('websites/create', $websiteParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$websiteId) {
            return;
        }

        // CREATE ITEM PARENT
        $itemParams = [
            'name' => 'Test',
            'parent_id' => $websiteId,
            'plural' => 'Tests',
            'techName' => 'test',
            'techPlural' => 'tests',
            'icon' => 'face',
            'descriptionColumn' => '3',
            'iconColumn' => '0',
            'defaultOrderColumn' => '3',
            'defaultOrderDirection' => '0',
            'gender' => '1',
            'deactivable' => '1',
            'sortable' => '1',
            'is_child' => '1',
            'parent' => '93',
        ];

        $itemId = $this->testApiRoute('items/create', $itemParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$itemId) {
            return;
        }

        // Test parameters attributes
        $attributeParams = [
            'num' => '0',
            'name' => 'Test',
            'parent_id' => $itemId,
            'techName' => 'test',
            'icon' => 'face',
            'type' => '',
            'orderable' => '1',
            'searchable' => '1',
            'is_unique' => '1',
            'nullable' => '1',
            'help' => '',
            'default_value' => '',
            'default_checked' => '1',
            'default_file' => '',
            'length' => '',
            'autocomplete' => '1',
            'strict' => '1',
            'is_float' => '1',
            'min' => '',
            'max' => '',
            'nb_integers' => '',
            'nb_decimals' => '',
            'step' => '',
            'use_box' => '1',
            'false_text' => '',
            'true_text' => '',
            'images' => '1',
            'embeds' => '1',
            'titles' => '1',
            'font_sizes' => '1',
            'reorder' => '1',
            'max_size' => '',
            'max_height' => '',
            'max_width' => '',
            'comp_jpg' => '1',
            'comp_webp' => '1',
            'interlace' => '1',
            'comp_png' => '1',
            'mime_types' => '',
            'rows_managment' => '1',
            'multiple' => '1',
            'is_range' => '1',
            'chips' => '1',
            'search' => '1',
            'values_source' => '',
            'values_table' => '',
            'values_file' => '',
            'values_text_attr' => '',
            'values_value_attr' => '',
            'values_icon_attr' => '',
            'values_disabled_attr' => '',
            'values_text' => '',
            'values_value' => '',
            'values_icon' => '',
            'values_disabled' => '',
            'values_use_values' => '1',
            'values_use_icons' => '1',
            'values_use_disabled' => '1',
            'csv_separator' => '',
            'select_type' => '',
            'reference' => '92',
            'values_reference' => '',
            'values_ref_text_attr' => '',
            'values_ref_value_attr' => '',
            'values_ref_icon_attr' => '',
            'values_ref_disabled_attr' => '3',
            'password_strength' => '1',
            'password_check' => '1',
        ];

        // Witness
        $attributeWitness = new Attribute($attributeParams);
        if ($attributeWitness == null) {
            $this->setError("Erreur lors de la création de l'attribut témoin");
            return;
        }

        // CREATE SUCCESS
        // Create an attribute
        $attributId = $this->testApiRoute('attributes/create', $attributeParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$attributId) {
            return;
        }

        // Check attribute exists
        $sql = "SELECT * FROM `tests_" . Attribute::TABLE . "` WHERE id = ?";
        $result = getOneFromDatabase($sql, $attributId);
        if (is_a($result, "OroshiError")) {
            $this->setError("Erreur SQL lors de la récupération de l'attribut :<br>$result->message");
            return;
        }

        // Check attributes
        unset($result["id"]);
        $attribute = new Attribute($result);
        if ($this->compareObjects($attributeWitness, $attribute)) {
            return;
        }

        // EDIT SUCCESS

        // Update witness
        $attributeWitness->name = "Test2";
        $attributeWitness->techName = "test";

        // Edit attributes
        $attributeParams = [
            'num' => '0',
            'name' => 'Test2',
            'parent_id' => $itemId,
            'techName' => 'test',
            'icon' => 'face',
            'type' => '',
            'orderable' => '1',
            'searchable' => '1',
            'is_unique' => '1',
            'nullable' => '1',
            'help' => '',
            'default_value' => '',
            'default_checked' => '1',
            'default_file' => '',
            'length' => '',
            'autocomplete' => '1',
            'strict' => '1',
            'is_float' => '1',
            'min' => '',
            'max' => '',
            'nb_integers' => '',
            'nb_decimals' => '',
            'step' => '',
            'use_box' => '1',
            'false_text' => '',
            'true_text' => '',
            'images' => '1',
            'embeds' => '1',
            'titles' => '1',
            'font_sizes' => '1',
            'reorder' => '1',
            'max_size' => '',
            'max_height' => '',
            'max_width' => '',
            'comp_jpg' => '1',
            'comp_webp' => '1',
            'interlace' => '1',
            'comp_png' => '1',
            'mime_types' => '',
            'rows_managment' => '1',
            'multiple' => '1',
            'is_range' => '1',
            'chips' => '1',
            'search' => '1',
            'values_source' => '',
            'values_table' => '',
            'values_file' => '',
            'values_text_attr' => '',
            'values_value_attr' => '',
            'values_icon_attr' => '',
            'values_disabled_attr' => '',
            'values_text' => '',
            'values_value' => '',
            'values_icon' => '',
            'values_disabled' => '',
            'values_use_values' => '1',
            'values_use_icons' => '1',
            'values_use_disabled' => '1',
            'csv_separator' => '',
            'select_type' => '',
            'reference' => '92',
            'values_reference' => '',
            'values_ref_text_attr' => '',
            'values_ref_value_attr' => '',
            'values_ref_icon_attr' => '',
            'values_ref_disabled_attr' => '3',
            'password_strength' => '1',
            'password_check' => '1',
            'id' => $attributId
        ];

        if (!$this->testApiRoute('attributes/edit', $attributeParams)) {
            return;
        }

        // Check attribute exists
        $sql = "SELECT * FROM `tests_" . Attribute::TABLE . "` WHERE id = ?";
        $result = getOneFromDatabase($sql, $attributId);
        if (is_a($result, "OroshiError")) {
            $this->setError("Erreur SQL lors de la récupération de l'attribut :<br>$result->message");
            return;
        }

        // Check attribute attributes
        unset($result["id"]);
        $attribute = new Attribute($result);
        if ($this->compareObjects($attributeWitness, $attribute)) {
            return;
        }

        // EDIT ERROR
        // Create an attribute invalid with the API
        if ($this->testApiRoute('attributes/edit', ['id' => $attributId], API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("Une erreur était attendue lors de l'édition d'un attribut invalide");
            return;
        }

        // CREATE ERROR
        // Create an attribute invalid with the API
        if ($this->testApiRoute('attributes/create', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("Une erreur était attendue lors de la création d'un attribut invalide");
            return;
        }

        // DELETE SUCCESS
        // Delete the attribute created via the API
        $deleteData = [
            'id' => $attributId
        ];

        if (!$this->testApiRoute('attributes/delete', $deleteData)) {
            return;
        }

        // DELETE ERROR
        // Try to delete the attribute again
        if ($this->testApiRoute('attributes/delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("On aurait du avoir une erreur lors de la 2éme suppression de l'attribut");
            return;
        }

        // DELETE ITEM PARENT
        // Delete the item created via the API
        $deleteData = [
            'id' => $itemId
        ];

        if (!$this->testApiRoute('items/delete', $deleteData)) {
            return;
        }

        // DELETE WEBSITE PARENT
        // Delete the website created via the API
        $deleteData = [
            'id' => $websiteId
        ];

        if (!$this->testApiRoute('websites/delete', $deleteData)) {
            return;
        }
    }
}
