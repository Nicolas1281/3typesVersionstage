<?php

include_once INSTALL_ROOT . "/managers/WebsitesManager.php";

class WebsitesTest extends Test {
    public $name = "Test Site";
    public $priority = 3;

    public function run() {

        // Test parameters website
        $websiteParams = [
            'name' => 'teste',
            'api_key' => '',
        ];

        // Witness
        $websiteWitness = new Website($websiteParams);
        if ($websiteWitness == null) {
            $this->setError("Erreur lors de la création du site témoin");
            return;
        }

        // CREATE SUCCESS
        // Create a website
        $websiteId = $this->testApiRoute('websites/create', $websiteParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$websiteId) {
            return;
        }

        // Check website exists
        $sql = "SELECT * FROM `tests_" . Website::TABLE . "` WHERE id = ?";
        $result = getOneFromDatabase($sql, $websiteId);
        if (is_a($result, "OroshiError")) {
            $this->setError("Erreur SQL lors de la récupération du site :<br>$result->message");
            return;
        }

        // Check website attributes
        unset($result["id"]);
        $website = new Website($result);
        if ($this->compareObjects($websiteWitness, $website)) {
            return;
        }

        // EDIT SUCCES
        // Update witness
        $websiteWitness->name = "Test2";
        $websiteWitness->api_key = "";

        // Edit website
        $websiteParams = [
            'name' => 'Test2',
            'api_key' => '',
            'id' => $websiteId
        ];

        if (!$this->testApiRoute('websites/edit', $websiteParams)) {
            return;
        }

        // Check website exists
        $sql = "SELECT * FROM `tests_" . Website::TABLE . "` WHERE id = ?";
        $result = getOneFromDatabase($sql, $websiteId);
        if (is_a($result, "OroshiError")) {
            $this->setError("Erreur SQL lors de la récupération du site :<br>$result->message");
            return;
        }

        // Check website attributes
        unset($result["id"]);
        $website = new Website($result);
        if ($this->compareObjects($websiteWitness, $website)) {
            return;
        }

        // EDIT ERROR
        // Create a website invalid with the API
        if ($this->testApiRoute('websites/edit', ['id' => $websiteId], API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("Une erreur était attendue lors de l'édition d'un site invalide");
            return;
        }

        // CREATE ERROR
        // Create a website invalid with the API
        if ($this->testApiRoute('websites/create', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("Une erreur était attendue lors de la création d'un site invalide");
            return;
        }

        // DELETE SUCCESS
        // Delete the website created via the API
        $deleteData = [
            'id' => $websiteId
        ];

        if (!$this->testApiRoute('websites/delete', $deleteData)) {
            return;
        }

        // DELETE ERROR
        // Try to delete the website again
        if ($this->testApiRoute('websites/delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("On aurait du avoir une erreur lors de la 2éme suppression du site");
            return;
        }
    }
}
