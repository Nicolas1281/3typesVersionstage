<?php

include_once INSTALL_ROOT . "/managers/AccountsManager.php";

class AccountTest extends Test {
    public $name = "Test Inscription";
    public $priority = 1;

    public function run() {

        $login = "test_" . uniqid();
        $email = $login . '@gmail.com';

        // Get the first available role
        $sql = "SELECT * FROM roles LIMIT 1";
        $result = getOneFromDatabase($sql);
        if ($result == null) {
            $this->setError("Aucun rôle trouvé");
            return;
        }
        $roleId = $result["id"];

        $accountParams = [
            'firstname' => 'Jean',
            'lastname' => 'Dupont',
            'login' => $login,
            'email' => $email,
            "role" => $roleId,
            "password" => "password",
            "password_check" => "password"
        ];

        // Witness
        $accountWitness = new Account($accountParams);
        if ($accountWitness == null) {
            $this->setError("Erreur lors de la création du compte témoin");
            return;
        }

        // CREATE SUCCESS
        // Create an account
        $accountId = $this->testApiRoute('accounts/create', $accountParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$accountId) {
            return;
        }

        // Check account exists
        $sql = "SELECT * FROM `tests_" . Account::TABLE . "` WHERE id = ?";
        $result = getOneFromDatabase($sql, $accountId);
        if (is_a($result, "OroshiError")) {
            $this->setError("Erreur SQL lors de la récupération du compte :<br/>$result->message");
            return;
        }

        // Check account attributes
        unset($result["id"]);
        $account = new Account($result);
        if ($this->compareObjects($accountWitness, $account)) {
            return;
        }

        // EDIT SUCCESS
        // Update witness
        $accountWitness->firstname = "Jean2";
        $accountWitness->lastname = "Dupont2";
        $accountWitness->login = $login . "2";
        $accountWitness->email = $email . "2";

        // Edit account
        $accountParams = [
            'firstname' => 'Jean2',
            'lastname' => 'Dupont2',
            'login' => $login . "2",
            'email' => $email . "2",
            'role' => $roleId,
            'id' => $accountId
        ];

        if (!$this->testApiRoute('accounts/edit', $accountParams)) {
            return;
        }

        // Check account attributes
        $sql = "SELECT * FROM `tests_" . Account::TABLE . "` WHERE id = ?";
        $result = getOneFromDatabase($sql, $accountId);
        if (is_a($result, "OroshiError")) {
            $this->setError("Erreur SQL lors de la récupération du compte :<br/>$result->message");
            return;
        }
        unset($result["id"]);
        $account = new Account($result);
        if ($this->compareObjects($accountWitness, $account)) {
            return;
        }

        // EDIT ERROR
        // Edit an account invalid with the API
        if ($this->testApiRoute('accounts/edit', ['id' => $accountId], API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("Une erreur était attendue lors de l'édition d'un compte invalide");
            return;
        }

        // CREATE ERROR
        // Create an account invalid with the API
        if ($this->testApiRoute('accounts/create', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("Une erreur était attendue lors de la création d'un compte invalide");
            return;
        }

        // DELETE SUCCESS
        // Delete the account created via the API
        $deleteData = [
            'id' => $accountId
        ];

        if (!$this->testApiRoute('accounts/delete', $deleteData)) {
            return;
        }

        // DELETE ERROR
        // Try to delete the account again
        if ($this->testApiRoute('accounts/delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("On aurait du avoir une erreur lors de la 2éme suppression du compte");
            return;
        }
    }
}
