<?php

include_once INSTALL_ROOT . "/managers/RolesManager.php";
include_once INSTALL_ROOT . "/managers/AccountsManager.php";

class RolesItemsTest extends Test {
    public $name = "Test d'accès au objet";
    public $priority = 7;

    public function run() {
        $accountId = 1;

        // Saving the current role
        $sql = "SELECT * FROM `tests_" . DB_PREFIX . "accounts` WHERE id = ?";
        $account = getOneFromDatabase($sql, $accountId);

        if (!$account) {
            $this->setError("Le compte avec l'ID 1 n'a pas été trouvé");
            return;
        }

        $savePreviousRole = $account['role'];

        // parameters role 0
        $roleParams = [
            'name' => 'Dev',
            'accounts' => '2',
            'roles' => '2',
            'health' => '2',
            "tests" => '2',
            "websites" => "2",
            "items" => "0",
            "attributes" => "2"
        ];

        $roleId = $this->testApiRoute('roles/create', $roleParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$roleId) {
            return;
        }

        // Change role to 0 (lowest access level) via API
        $accountParams = [
            'id' => $accountId,
            'firstname' => 'Test',
            'lastname' => 'Agent',
            'login' => 'test',
            'email' => 'test@test.com',
            'role' => $roleId
        ];

        if (!$this->testApiRoute('accounts/edit', $accountParams)) {
            return;
        }

        // CREATE WEBSITE PARENT
        // Create a website
        $websiteParams = [
            'name' => 'teste',
            'api_key' => '',
        ];

        $websiteId = $this->testApiRoute('websites/create', $websiteParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$websiteId) {
            return;
        }

        // Role 0 tests create
        $itemParams = [
            'name' => 'Test',
            'parent_id' => $websiteId,
            'plural' => 'Tests',
            'techName' => 'test',
            'techPlural' => 'tests',
            'icon' => 'face',
            'descriptionColumn' => '3',
            'iconColumn' => '0',
            'defaultOrderColumn' => '3',
            'defaultOrderDirection' => '0',
            'gender' => '1',
            'deactivable' => '1',
            'sortable' => '1',
            'is_child' => '1',
            'parent' => '93',
        ];

        if ($this->testApiRoute('items/create', $itemParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/items/create fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests edit
        $itemParams['name'] = 'test2'; // Change the name parameter for editing

        if ($this->testApiRoute('items/edit', $itemParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/items/edit fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests screen
        $params = [
            'id' => '1',
            'screen' => 'editForm'
        ];

        if ($this->testApiRoute('items/screen', $params, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/items/screen fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests delete
        if ($this->testApiRoute('items/delete', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/items/delete fonctionne pour le rôle à 0");
            return;
        }

        // Role 0 tests multiple_delete
        $deleteData = [
            'id' => [1, 2, 3]
        ];
        if ($this->testApiRoute('items/multiple_delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/items/multiple_delete fonctionne pour le rôle à 0");
            return;
        }

        // Edit the role 1 via API
        $roleParams['items'] = '1'; // Change the items parameter to '1' for editing
        $roleParams['id'] = $roleId;

        if (!$this->testApiRoute('roles/edit', $roleParams)) {
            return;
        }

        // Role 1 tests create
        if ($this->testApiRoute('items/create', $itemParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/items/create fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests edit
        $itemParams['name'] = 'test2'; // Change the name parameter for editing

        if ($this->testApiRoute('items/edit', $itemParams, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/items/edit fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests screen
        $params = [
            'id' => '1',
            'screen' => 'editForm'
        ];

        if (!$this->testApiRoute('items/screen', $params)) {
            return;
        }

        // Role 1 tests list
        $params = [
            'count' => '100',
            'page' => '0',
            'order' => 1
        ];
        if (!$this->testApiRoute('items/list', $params)) {
            return;
        }

        // Role 1 tests delete
        if ($this->testApiRoute('items/delete', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/items/delete fonctionne pour le rôle à 1");
            return;
        }

        // Role 1 tests multiple_delete
        $deleteData = [
            'id' => [1, 2, 3]
        ];
        if ($this->testApiRoute('items/multiple_delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("La route api/items/multiple_delete fonctionne pour le rôle à 1");
            return;
        }

        // Edit the role 2 via API
        $roleParams['items'] = '2'; // Change the items parameter to '2' for editing
        $roleParams['id'] = $roleId;

        if (!$this->testApiRoute('roles/edit', $roleParams)) {
            return;
        }

        // Role 2 tests create
        $itemParams = [
            'name' => 'Test',
            'parent_id' => $websiteId,
            'plural' => 'Tests',
            'techName' => 'test',
            'techPlural' => 'tests',
            'icon' => 'face',
            'descriptionColumn' => '3',
            'iconColumn' => '0',
            'defaultOrderColumn' => '3',
            'defaultOrderDirection' => '0',
            'gender' => '1',
            'deactivable' => '1',
            'sortable' => '1',
            'is_child' => '1',
            'parent' => '93',
        ];

        $itemId = $this->testApiRoute('items/create', $itemParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$itemId) {
            return;
        }

        // Role 2 tests edit
        $itemParams['name'] = 'Test2';
        $itemParams['id'] = $itemId;

        if (!$this->testApiRoute('items/edit', $itemParams)) {
            return;
        }

        // Role 2 tests screen
        $params = [
            'id' => $itemId,
            'screen' => 'editForm'
        ];

        if (!$this->testApiRoute('items/screen', $params)) {
            return;
        }

        // Role 2 tests list
        $params = [
            'count' => '100',
            'page' => '0',
            'order' => 1
        ];

        if (!$this->testApiRoute('items/list', $params)) {
            return;
        }

        // Role 2 tests delete
        $deleteData = [
            'id' => $itemId
        ];

        if (!$this->testApiRoute('items/delete', $deleteData)) {
            return;
        }

        // Role 2 tests createMultipledelete1
        $itemParams = [
            'name' => 'Test1',
            'parent_id' => $websiteId,
            'plural' => 'Tests',
            'techName' => 'test',
            'techPlural' => 'tests',
            'icon' => 'face',
            'descriptionColumn' => '3',
            'iconColumn' => '0',
            'defaultOrderColumn' => '3',
            'defaultOrderDirection' => '0',
            'gender' => '1',
            'deactivable' => '1',
            'sortable' => '1',
            'is_child' => '1',
            'parent' => '93',
        ];

        $itemsIdOne = $this->testApiRoute('items/create', $itemParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$itemsIdOne) {
            return;
        }

        // Role 2 tests createMultipledelete2
        $itemParams = [
            'name' => 'Test2',
            'parent_id' => $websiteId,
            'plural' => 'Tests',
            'techName' => 'test',
            'techPlural' => 'tests',
            'icon' => 'face',
            'descriptionColumn' => '3',
            'iconColumn' => '0',
            'defaultOrderColumn' => '3',
            'defaultOrderDirection' => '0',
            'gender' => '1',
            'deactivable' => '1',
            'sortable' => '1',
            'is_child' => '1',
            'parent' => '93',
        ];

        $itemsIdTwo = $this->testApiRoute('items/create', $itemParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$itemsIdTwo) {
            return;
        }

        // Role 2 tests multiple_delete
        $deleteData = [
            'id' => "$itemsIdOne,$itemsIdTwo"
        ];

        if (!$this->testApiRoute('items/multiple_delete', $deleteData)) {
            return;
        }

        // DELETE WEBSITE PARENT
        // Delete the website created via the API
        $deleteData = [
            'id' => $websiteId
        ];

        if (!$this->testApiRoute('websites/delete', $deleteData)) {
            return;
        }

        // Restoring the role via API
        $accountParams['role'] = $savePreviousRole;

        if (!$this->testApiRoute('accounts/edit', $accountParams)) {
            return;
        }

        // Delete the role created via the API
        $deleteData = [
            'id' => $roleId
        ];

        if (!$this->testApiRoute('roles/delete', $deleteData)) {
            return;
        }
    }
}
