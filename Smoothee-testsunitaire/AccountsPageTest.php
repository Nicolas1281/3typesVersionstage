<?php

class AccountsPageTest extends Test {
  public $name = "Test d'erreur dans une page";

  public function run() {
    $response = sendForm(LOCAL_SERVER . INSTALL_DIR . '/accounts');

    $ch = getConnection();
    $httpReturnCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($httpReturnCode != 200) {
      $this->setError("La page <i>accounts</i> à renvoyer le code HTTP " . $httpReturnCode);
      return;
    }

    if (strlen($response) == 0) {
      $this->setError("La page <i>accounts</i> à renvoyer une réponse vide");
      return;
    }

    $matches = null;
    preg_match("/(<table class='xdebug.*<\/table>)/is", $response, $matches);
    if (count($matches) == 2) {
      $this->setError("Erreur dans la pages <i>accounts</i>:<br/><br/><font size='1'>" . $matches[1] . "</font>");
      return;
    }
  }
}
