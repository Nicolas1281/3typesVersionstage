<?php

include_once INSTALL_ROOT . "/managers/ItemsManager.php";

class ItemsTest extends Test {
    public $name = "Test objet";
    public $priority = 4;

    public function run() {

        // CREATE WEBSITE PARENT
        // Create a website
        $websiteParams = [
            'name' => 'teste',
            'api_key' => '',
        ];

        $websiteId = $this->testApiRoute('websites/create', $websiteParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$websiteId) {
            return;
        }

        // Test parameters item
        $itemParams = [
            'name' => 'Test',
            'parent_id' => $websiteId,
            'plural' => 'Tests',
            'techName' => 'test',
            'techPlural' => 'tests',
            'icon' => 'face',
            'descriptionColumn' => '3',
            'iconColumn' => '0',
            'defaultOrderColumn' => '3',
            'defaultOrderDirection' => '0',
            'gender' => '1',
            'deactivable' => '1',
            'sortable' => '1',
            'is_child' => '1',
            'parent' => '93',
        ];

        // Witness
        $itemWitness = new Item($itemParams);
        if ($itemWitness == null) {
            $this->setError("Erreur lors de la création de l'objet témoin");
            return;
        }

        // CREATE SUCCESS

        // Create an item
        $itemId = $this->testApiRoute('items/create', $itemParams, API_STATUS_SUCCESS_JSON, true, $jsonData);

        if (!$itemId) {
            return;
        }

        // Check item exists
        $sql = "SELECT * FROM `tests_" . Item::TABLE . "` WHERE id = ?";
        $result = getOneFromDatabase($sql, $itemId);
        if (is_a($result, "OroshiError")) {
            $this->setError("Erreur SQL lors de la récupération de l'objet :<br>$result->message");
            return;
        }

        // Check item attributes
        unset($result["id"]);
        $item = new Item($result);
        if ($this->compareObjects($itemWitness, $item)) {
            return;
        }

        // EDIT SUCCESS
        // Update witness
        $itemWitness->name = "Test2";
        $itemWitness->plural = "Tests2";
        $itemWitness->techName = "test";
        $itemWitness->techPlural = "tests";
        $itemWitness->descriptionColumn = "2";
        $itemWitness->defaultOrderColumn = "2";

        // Edit item
        $itemParams = [
            'name' => 'Test2',
            'parent_id' => $websiteId,
            'plural' => 'Tests2',
            'techName' => 'test',
            'techPlural' => 'tests',
            'icon' => 'face',
            'descriptionColumn' => '2',
            'iconColumn' => '0',
            'defaultOrderColumn' => '2',
            'defaultOrderDirection' => '0',
            'gender' => '1',
            'deactivable' => '1',
            'sortable' => '1',
            'is_child' => '1',
            'parent' => '93',
            'id' => $itemId
        ];

        if (!$this->testApiRoute('items/edit', $itemParams)) {
            return;
        }

        // Check item exists
        $sql = "SELECT * FROM `tests_" . Item::TABLE . "` WHERE id = ?";
        $result = getOneFromDatabase($sql, $itemId);
        if (is_a($result, "OroshiError")) {
            $this->setError("Erreur SQL lors de la récupération de l'objet :<br>$result->message");
            return;
        }

        // Check item attributes
        unset($result["id"]);
        $item = new Item($result);
        if ($this->compareObjects($itemWitness, $item)) {
            return;
        }

        // EDIT ERROR
        // Create an item invalid with the API
        if ($this->testApiRoute('items/edit', ['id' => $itemId], API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("Une erreur était attendue lors de l'édition d'un objet invalide");
            return;
        }

        // CREATE ERROR
        // Create an item invalid with the API
        if ($this->testApiRoute('items/create', null, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("Une erreur était attendue lors de la création d'un objet invalide");
            return;
        }

        // DELETE SUCCESS
        // Delete the item created via the API
        $deleteData = [
            'id' => $itemId
        ];

        if (!$this->testApiRoute('items/delete', $deleteData)) {
            return;
        }

        // DELETE ERROR
        // Try to delete the item again
        if ($this->testApiRoute('items/delete', $deleteData, API_STATUS_SUCCESS_JSON, false)) {
            $this->setError("On aurait du avoir une erreur lors de la 2éme suppression de l'objet");
            return;
        }

        // DELETE WEBSITE PARENT
        // Delete the website created via the API
        $deleteData = [
            'id' => $websiteId
        ];

        if (!$this->testApiRoute('websites/delete', $deleteData)) {
            return;
        }
    }
}
