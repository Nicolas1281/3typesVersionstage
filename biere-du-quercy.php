<?php
$menu = "none";
$title = "QB - 3Types";
$bodyback = "##0C2B2B";
$project = "qb";
include "head.php";
?>
<script src="asset/js/shuffle.min.js"></script>
<script src="asset/js/gsap.min.js"></script>
<script src="asset/js/doublebutton.js"></script>

<img style="display:none;" src="asset/img/qb/image1.jpg" />
<img style="display:none;" src="asset/img/qb/image1.jpg" />
<div class="project-body" style="background-color: #0C2B2B;">
    <div class="project-first">
        <div class="project-first-inner project-first-desktop" style="background-image: url('asset/img/qb/image1.jpg');" border="151515">
            <img class="floating-arrow" id="arrow_scroll" src="asset/img/arrow_light.svg" />
        </div>
        <div class="project-first-inner project-first-mobile" style="background-image: url('asset/img/qb/image1.jpg');" border="151515">
            <img class="floating-arrow" id="arrow_scroll2" src="asset/img/arrow_light.svg" />
        </div>
    </div>

    <div class="project-info" id="project_info">
        <div class="project-client project-client-white">
            <h2>Client</h2>
            <hr>
            <p>
                Situé dans le Quercy, le domine de Merchien se lance dans la production de bière !
                Nous avons réalisé l’identité visuelle ainsi que les étiquettes des différentes bière produites et brassées à la main sur le domaine.
            </p>
        </div>
        <div class="project-keywords project-keywords-white">
            <span>05</span>
            <h2>Mots clés</h2>
            <hr>
            <div>
                <p>Branding / Logotype / Direction artistique / Identité visuelle / Packaging </p>
                <strong>Brasserie</strong>
            </div>
        </div>
    </div>

    <div class="project-two-grid">
        <div class="qb-left">
            <img src="asset/img/qb/image3.svg" />
            <p>
                Identité Visuelle <br>
                Packaging <br>
                design graphic
            </p>
            <span>
                Bière biologique <br>
                brassée à la main
            </span>
        </div>
        <div class="qb-right">
            <img src="asset/img/qb/image2.jpg" />
        </div>
    </div>

    <div class="qb-center">
        <img src="asset/img/qb/image4.svg" alt="">
        <img src="asset/img/qb/image4.png" />
    </div>

    <div class="qb">
        <img src="asset/img/qb/image5.jpg" alt="">
    </div>

    <div class="project-two-grid" style="background-color: #3A0D15;">
        <div class="qb-left2">
            <img src="asset/img/qb/image6.png" />
        </div>
        <div class="qb-right2">
            <img src="asset/img/qb/image7.svg" />
            <div>
                <p>
                    Une affaire DE FAMILLE
                </p>
                <p>
                    La bière du Quercy (QB) a vu le jour en 2012.
                    Elle est brassée à la main par Zakarie Meakean au domaine de Merchien créé par ses parents en 1997.
                </p>
            </div>
            <span>
                Domaine de Merchien <br>
                Lot et Garone
            </span>
        </div>
    </div>

    <div class="qb-image-center">
        <img src="asset/img/qb/image8.svg" />
    </div>

    <div class="qb-relative">
        <img src=" asset/img/qb/image9.jpg" data-aos="fade-right" data-aos-duration="500" data-aos-delay="500" style="width: 100%;">
        <img src="asset/img/qb/image18.svg" alt="">
    </div>

    <div class="qb-image-center2">
        <p>6 bières, 6 ambiances</p>
    </div>

    <div class="project-six-grid">
        <div class="qb-blanche">
            <p>BLANCHE</p>
        </div>
        <div class="qb-blonde">
            <p>BLONDE</p>
        </div>
        <div class="qb-blondesafran">
            <p>BLONDE SAFRAN</p>
        </div>
        <div class="qb-brune">
            <p>BRUNE</p>
        </div>
        <div class="qb-ipa">
            <p>IPA</p>
        </div>
        <div class="qb-ambree">
            <p>AMBRÉE</p>
        </div>
    </div>

    <div class="project-six-grid">
        <div class="qb-blanche">
            <img src="asset/img/qb/image10.png" alt="">
        </div>
        <div class="qb-blonde">
            <img src="asset/img/qb/image11.png" alt="">
        </div>
        <div class="qb-blondesafran">
            <img src="asset/img/qb/image12.png" alt="">
        </div>
        <div class="qb-brune">
            <img src="asset/img/qb/image13.png" alt="">
        </div>
        <div class="qb-ipa">
            <img src="asset/img/qb/image14.png" alt="">
        </div>
        <div class="qb-ambree">
            <img src="asset/img/qb/image15.png" alt="">
        </div>
    </div>

    <div class="qb-image-padding2">
        <img src="asset/img/qb/image16.jpg" data-aos="fade-right" data-aos-duration="500" data-aos-delay="500" />
    </div>

    <div class="qb-image-center3">
        <div style="position: relative;">
            <img src="asset/img/qb/image17.jpg" />
            <div class="project-bottom" style="position: absolute; bottom: -12px; width: 100%; box-sizing: border-box;">
                <div class="project-bottom-block">
                    <h2>Bière du Quercy</h2>
                </div>
                <div class="project-bottom-block">
                    <h2>Brasserie Artisanale</h2>
                </div>
                <div class="project-bottom-block">
                    <h2>2022</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="project-bottom-button" style="background-color: #0F0F0F;">
        <a href="projets" class="double-button studio-button-center">
            <div class="double-button-back">
                RETOUR&nbsp;AUX&nbsp;PROJETS
            </div>
            <p class="double-button-text">
                RETOUR&nbsp;AUX&nbsp;PROJETS
            </p>
        </a>
    </div>
</div>

<script src="asset/js/project.js"></script>
<?php
include "foot.php";
?>