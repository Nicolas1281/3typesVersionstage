function openJob(event, id = -1) {
    if (id == -1) {
      var { event, id } = processObjectContextMenuAction(event);
    }
  
    // Construire l'URL en ajoutant l'ID à la fin
    var url = `https://www.grainesetcompetences.fr/offre?id=${id}`;
  
    // Fermer les tooltips et verrouiller d'autres éléments si nécessaire
    closeTooltips();

    // Ouvrir la nouvelle fenêtre ou onglet
    window.open(url, '_blank');
  
  }
  