<?php
  $title = "Hugo Zely | legals";
  $description = "";
  $menu = "legals";
  include_once $_SERVER['DOCUMENT_ROOT']."/head.php";
?>

<div class="page-content">
    <div class="mentions-body">
    <h1>LEGAL NOTICE</h1>
    <p>Please read these terms of use of this site carefully before browsing it. By connecting to this site, you fully accept these terms and conditions.</p>

    <h2>SITE EDITOR</h2>
    <p>
      <strong>Hugo Zely</strong><br>
      Toulouse, France<br>
      <a href="mailto:hello@hugozely.fr">hello@hugozely.fr</a><br>
      <a href="tel:+3366331715">+33 (0)6 66 33 17 15</a><br>
      <a href="https://hugozely.fr">hugozely.fr</a>
    </p>

    <h2>SITE DESIGN AND MANAGEMENT</h2>
    <p>
        The editorial and graphic monitoring, as well as the technical and ergonomic design of the site, are provided by the 3Types studio: www.3types.fr
    </p>

    <h2>TERMS OF USE</h2>
    <p>
        he site accessible by the url www.mecano-id.fr is operated in compliance with French legislation. The use of this site is governed by these general conditions. 
        By using the site, you acknowledge having read these conditions and have accepted them. These can be modified at any time and without notice by Hugo Zely. 
        Hugo Zely cannot be held responsible in any way for misuse of the service.
    </p>

    <h2>LIMITATION OF LIABILITY</h2>
    <p>
        The information on this site is as accurate as possible and the site is periodically updated, but may contain inaccuracies, omissions or gaps. 
        If you notice a gap, error or what appears to be a malfunction, please report it by email, describing the problem as precisely as possible 
        (page causing the problem, triggering action, type of computer and browser used…).
        Any downloaded content is done at the user’s own risk and under his sole responsibility. 
        Consequently, Hugo Hely cannot be held responsible for any damage to the user’s computer or any loss of data resulting from the download.
        The photos are not contractual.
        The hypertext links set up within the framework of this website to other sources present on the Internet network can not engage the responsibility of Hugo Zely
    </p>

    <h2>LITIGATION</h2>
    <p>
        These conditions are governed by French law and any dispute or litigation that may arise from the interpretation or execution 
        of these will be the exclusive jurisdiction of the courts on which Hugo Zely depends. 
        The reference language for settling any disputes is French.
    </p>

    <h2>RIGHT OF ACCESS</h2>
    <p>
        Pursuant to this law, Internet users have the right to access, rectify, modify and delete data concerning them personally. This right can be exercised electronically at 
        the following email address: hello@hugozely.fr.
        The personal information collected is under no circumstances entrusted to third parties.
        The information collected about you is processed for: Hugo Zely
        For the following purpose: Quotation, request for information, application and web traffic analysis.
        The recipients of these data are: Our team.
        The data retention period is 6 years after registration and 4 years for customer data.
        You have the right to access, rectify and port , erasure of these or limitation of processing.
        You can object to the processing of your personal data and have the right to withdraw your consent at any time by contacting: hello@hugozely.fr.
        You have the possibility to lodge a complaint with a supervisory authority.
        Since the law “Informatique et Libertés” of January 6, 1978 as amended, you have the right to access and rectify information which you provide. concern. 
        If you wish to exercise this right and obtain information about you, please contact hello@hugozely.fr.
        You can also, for legitimate reasons, oppose the processing of your data.
    </p>
    
    <h2>CONFIDENTIALITY</h2>
    <p>
        Your personal data is confidential and will under no circumstances be communicated to third parties.
    </p>

    <h2>INTELLECTUAL PROPERTY</h2>
    <p>
        All the content of this site, including, without limitation, graphics, images, texts, videos, animations, sounds, logos, gifs and icons as well as their formatting 
        are the exclusive property of Hugo Zely at l Except for brands, logos or content belonging to other partner companies or authors.
        Any reproduction, distribution, modification, adaptation, retransmission or publication, even partial, of these various elements is strictly prohibited without 
        the express consent of written from Hugo Zely. This representation or reproduction, by any means whatsoever, constitutes an infringement punishable by articles L.3335-2 and 
        following of the Code of intellectual property. Failure to comply with this prohibition constitutes an infringement which may engage the civil and criminal liability of 
        the infringer. In addition, the owners of the copied content could take legal action against you.
        Hugo Zely is the identical owner of the “rights of database producers” referred to in Book III, Title IV, of the Intellectual Property Code (law n ° 98-536 of July 1, 1998) 
        relating to copyright and databases.
        Users and visitors of the website can set up a hyperlink to this site.
        For any authorization or information request, please contact us by email: hello@hugozely.fr. Specific conditions are foreseen for the press.
        In addition, the formatting of this site required the use of external sources for which we have acquired the rights or whose rights of use are open: 
        three.js, gsap.js and jquery.js.
    </p>
   
    <h2>HOST</h2>
    <p>
        OVH<br>
        Host and telecommunications operator<br>
        www.ovh.com<br>
    </p>

    <h2>TERMS OF SERVICE</h2>
    <p>
        This site is available in HTML5 and CSS3 languages, for better ease of use and more pleasant graphics, 
        we recommend that you use modern browsers such as Safari, Firefox, Chrome, …
    </p>

    <h2>INFORMATION AND EXCLUSION</h2>
    <p>
        Hugo Zely implements all the means at its disposal to ensure reliable information and reliable updating of its websites. However, errors or omissions may occur. 
        The Internet user must therefore ensure the accuracy of the information with Hugo Zely, and report any changes to the site that he deems useful. 
        Hugo Zely is in no way responsible for the use made of this information, and for any direct or indirect damage that may result from it.
    </p>

    <h2>
      Cookies
    </h2>
    <p>
        For statistical and display purposes, this site uses cookies. These are small text files stored on your hard drive in order to record technical data on your navigation. 
        Some parts of this site may not be functional without the acceptance of cookies.
    </p>

    <h2>HYPERTEXT LINKS</h2>
    <p>
        The Hugo Zely website may offer links to other websites or other resources available on the Internet.
        Hugo Zely has no means to control the sites in connection with its websites. 
        Hugo Zely does not answer for the availability of such sites and external sources, nor does it guarantee it. 
        It cannot be held responsible for any damage, of any nature whatsoever, resulting from the content of these sites or external sources, and in particular 
        from the information, products or services they offer, or from any use that may be made of these elements. 
        The risks associated with this use fall fully on the Internet user, who must comply with their conditions of use.
    </p>

    <h2>PRECAUTIONS FOR USE</h2>
    <p>
        It is therefore your responsibility to take the necessary precautions to ensure that what you choose to use is not tainted with errors or even elements 
        of a destructive nature such as viruses, trojans, etc. .
    </p>

    <h2>RESPONSIBILITY</h2>
    <p>
        No other guarantee is given to the customer, who has the obligation to clearly formulate his needs and the duty to inform himself. 
        If the information provided by Hugo Zely appears to be inaccurate, it will be up to the customer to carry out any checks on the consistency or plausibility of 
        the results obtained. Hugo Zely will not be responsible in any way towards third parties for the use by the customer of 
        information or their absence contained in its products, including one of its websites.
    </p>

    <h2>CONTACT US</h2>
    <p>
        Hugo Zely is at your disposal for all your comments or suggestions. You can write to us in French by e-mail at: hello@hugozely.fr.
    </p>
    </div>
</div>

<?php
  $noprefoot = true;
  include_once $_SERVER['DOCUMENT_ROOT']."/foot.php";
?>