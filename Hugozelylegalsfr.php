<?php
  $title = "Hugo Zely | legals";
  $description = "";
  $menu = "legals";
  include_once $_SERVER['DOCUMENT_ROOT']."/head.php";
?>

<div class="page-content">
  <div class="mentions-body">
    <h1>Mentions légales</h1>
    <p>Merci de lire attentivement les présentes modalités d'utilisation du présent site avant de le
    parcourir. En vous connectant sur ce site, vous acceptez sans réserve les présentes modalités.</p>

    <h2>Éditeur du site</h2>
    <p>
      <strong>Hugo Zely</strong><br>
      Toulouse, France<br>
      <a href="mailto:hello@hugozely.fr">hello@hugozely.fr</a><br>
      <a href="tel:+3366331715">+33 (0)6 66 33 17 15</a><br>
      <a href="https://hugozely.fr">hugozely.fr</a>
    </p>

    <h2>CONCEPTION ET GESTION DU SITE</h2>
    <p>
      Le suivi éditorial et graphique ainsi que la conception technique et ergonomique du site sont assurés par le studio 3Types : www.3types.fr
    </p>

    <h2>Conditions d'utilisation</h2>
    <p>
      Le site accessible par l'url hugozely.fr est exploité dans le respect de la législation française.
      L'utilisation de ce site est régie par les présentes conditions générales.
      En utilisant le site, vous reconnaissez avoir pris connaissance de ces conditions et les avoir acceptées.
      Celles-ci pourront êtres modifiées à tout moment et sans préavis par le studio Hugo Zely.
      Le studio Hugo Zely ne saurait être tenu pour responsable en aucune manière d’une mauvaise utilisation du service.
    </p>

    <h2>Limitation de responsabilité</h2>
    <p>
      Les informations contenues sur ce site sont aussi précises que possibles et le site est périodiquement
      remis à jour, mais peut toutefois contenir des inexactitudes, des omissions ou des lacunes.
      Si vous constatez une lacune, erreur ou ce qui parait être un dysfonctionnement,
      merci de bien vouloir le signaler par email en décrivant le problème de la manière la plus précise
      possible (page posant problème, action déclenchante, type d’ordinateur et de navigateur utilisé, …).
    </p>
    <p>
      Tout contenu téléchargé se fait aux risques et périls de l'utilisateur et sous sa seule responsabilité.
      En conséquence, Hugo Zely ne saurait être tenu responsable d'un quelconque dommage subi par l'ordinateur de
      l'utilisateur ou d'une quelconque perte de données consécutives au téléchargement.
    </p>
    <p>Les photos sont non contractuelles.</p>
    <p>
      Les liens hypertextes mis en place dans le cadre du présent site internet en direction d'autres
      essources présentes sur le réseau Internet ne sauraient engager la responsabilité de Hugo Zely.
    </p>

    <h2>Litiges</h2>
    <p>
      Les présentes conditions sont régies par les lois françaises et toute contestation ou litiges qui pourraient naître
      de l'interprétation ou de l'exécution de celles-ci seront de la compétence exclusive des tribunaux dont dépend Hugo Zely.
      La langue de référence, pour le règlement de contentieux éventuels, est le français.
    </p>

    <h2>Droit d'accès</h2>
    <p>
      En application de cette loi, les internautes disposent d’un droit d’accès, de rectification, de modification et de
      suppression concernant les données qui les concernent personnellement. Ce droit peut être exercé par voie électronique
      à l’adresse email suivante : hello@hugozely.fr.<br>
      Les informations personnelles collectées ne sont en aucun cas confiées à des tiers.
    </p>
    <p>
      Les informations recueillies vous concernant font l’objet d’un traitement destiné à : Hugo Zely<br>
      Pour la finalité suivante : Devis, demande d'informations, candidature et analyse du trafic web.<br>
      Les destinataires de ces données sont : Notre équipe.<br>
      La durée de conservation des données est de 6 ans après l’inscription et de 4 ans pour les données clients.<br>
      Vous bénéficiez d’un droit d’accès, de rectification, de portabilité, d’effacement de celles-ci ou une limitation du traitement.<br>
      Vous pouvez vous opposer au traitement des données vous concernant et disposez du droit de retirer votre consentement
      à tout moment en vous adressant à : hello@hugozely.fr.<br>
      Vous avez la possibilité d’introduire une réclamation auprès d’une autorité de contrôle.<br>
      Depuis la loi “informatique et libertés” du 6 janvier 1978 modifiée, vous bénéficiez d’un droit d’accès et de
      rectification aux informations qui vous concernent. Si vous souhaitez exercer ce droit et obtenir communication
      des informations vous concernant, veuillez vous adresser à hello@hugozely.fr.<br>
      Vous pouvez également, pour des motifs légitimes, vous opposer au traitement des données vous concernant.
    </p>

    <h2>Confidentialité</h2>
    <p>
      Vos données personnelles sont confidentielles et ne seront en aucun cas communiquées à des tiers.
    </p>

    <h2>Propriété intellectuelle</h2>
    <p>
      Tout le contenu du présent site, incluant, de façon non limitative, les graphismes, images, textes, vidéos, animations,
      sons, logos, gifs et icônes ainsi que leur mise en forme sont la propriété exclusive de Hugo Zely
      à l'exception des marques, logos ou contenus appartenant à d'autres sociétés partenaires ou auteurs.<br>
      Toute reproduction, distribution, modification, adaptation, retransmission ou publication, même partielle, de ces
      différents éléments est strictement interdite sans l'accord exprès par écrit de Hugo Zely. Cette représentation
      ou reproduction, par quelque procédé que ce soit, constitue une contrefaçon sanctionnée par les articles L.3335-2 et
      suivants du Code de la propriété intellectuelle. Le non-respect de cette interdiction constitue une contrefaçon pouvant
      engager la responsabilité civile et pénale du contrefacteur. En outre, les propriétaires des contenus copiés pourraient
      intenter une action en justice à votre encontre.
    </p>
    <p>
      Hugo Zely est identiquement propriétaire des "droits des producteurs de bases de données" visés au Livre III,
      Titre IV, du Code de la Propriété Intellectuelle (loi n° 98-536 du 1er juillet 1998) relative aux droits d'auteur et aux bases de données.
    </p>
    <p>
      Les utilisateurs et visiteurs du site internet peuvent mettre en place un hyperlien en direction de ce site.
    </p>
    <p>
      Pour toute demande d'autorisation ou d'information, veuillez nous contacter par email : hello@hugozely.fr.
      Des conditions spécifiques sont prévues pour la presse.
    </p>
    <p>
      Par ailleurs, la mise en forme de ce site a nécessité le recours à des sources externes dont nous avons acquis les
      droits ou dont les droits d'utilisation sont ouverts : jquery.js, jquery.ripples.js, SmoothScroll.js, sortable.js.
    </p>

    <h2>Hébergeur</h2>
    <p>
      OVH<br>
      Hébergeur et opérateur de télécommunications<br>
      www.ovh.com<br>
    </p>

    <h2>Conditions de service</h2>
    <p>
      Ce site est proposé en langages HTML5 et CSS3, pour un meilleur confort d'utilisation et un graphisme plus agréable,
      nous vous recommandons de recourir à des navigateurs modernes comme Safari, Firefox, Chrome,...
    </p>

    <h2>Informations et exclusion</h2>
    <p>
      Hugo Zely met en œuvre tous les moyens dont elle dispose, pour assurer une information fiable et une mise
      à jour fiable de ses sites internet. Toutefois, des erreurs ou omissions peuvent survenir. L'internaute devra donc
      s'assurer de l'exactitude des informations auprès de Hugo Zely, et signaler toutes modifications du site
      qu'il jugerait utile. Hugo Zely n'est en aucun cas responsable de l'utilisation faite de ces informations,
      et de tout préjudice direct ou indirect pouvant en découler.
    </p>

    <h2>
      Cookies
    </h2>
    <p>
      Pour des besoins de statistiques et d'affichage, le présent site utilise des cookies. Il s'agit de petits fichiers
      textes stockés sur votre disque dur afin d'enregistrer des données techniques sur votre navigation. Certaines parties
      de ce site ne peuvent être fonctionnelle sans l’acceptation de cookies.
    </p>

    <h2>Liens hypertextes</h2>
    <p>
      Le site internet de Hugo Zely peut offrir des liens vers d’autres sites internet ou d’autres ressources
      disponibles sur Internet.
    </p>
    <p>
      Hugo Zely ne dispose d'aucun moyen pour contrôler les sites en connexion avec ses sites internet.
      Hugo Zely ne répond pas de la disponibilité de tels sites et sources externes, ni ne la garantit.
      Elle ne peut être tenue pour responsable de tout dommage, de quelque nature que ce soit, résultant du contenu de
      ces sites ou sources externes, et notamment des informations, produits ou services qu’ils proposent, ou de tout
      usage qui peut être fait de ces éléments. Les risques liés à cette utilisation incombent pleinement à l'internaute,
      qui doit se conformer à leurs conditions d'utilisation.
    </p>

    <h2>Précautions d'usage</h2>
    <p>
      Il vous incombe par conséquent de prendre les précautions d'usage nécessaires pour vous assurer que ce que vous
      choisissez d'utiliser ne soit pas entaché d'erreurs voire d'éléments de nature destructrice tels que virus, trojans, etc...
    </p>

    <h2>Responsabilité</h2>
    <p>
      Aucune autre garantie n'est accordée au client, auquel incombe l'obligation de formuler clairement ses besoins et
      le devoir de s'informer. Si des informations fournies par Hugo Zely apparaissent inexactes, il appartiendra
      au client de procéder lui-même à toutes vérifications de la cohérence ou de la vraisemblance des résultats obtenus.
      Hugo Zely ne sera en aucune façon responsable vis à vis des tiers de l'utilisation par le client des informations
      ou de leur absence contenues dans ses produits y compris un de ses sites internet.
    </p>

    <h2>Contactez-nous</h2>
    <p>
      Hugo Zely est à votre disposition pour tous vos commentaires ou suggestions.
      Vous pouvez nous écrire en français par courrier électronique à : hello@hugozely.fr.
    </p>
  </div>
</div>

<?php
  $noprefoot = true;
  include_once $_SERVER['DOCUMENT_ROOT']."/foot.php";
?>
