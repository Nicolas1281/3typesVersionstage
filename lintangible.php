<?php
$menu = "none";
$title = "L'Intangible - 3Types";
$bodyback = "#152E5F";
$project = "lintangible";
include "head.php";
?>
<script src="asset/js/shuffle.min.js"></script>
<script src="asset/js/gsap.min.js"></script>
<script src="asset/js/doublebutton.js"></script>
<img style="display:none;" src="asset/img/lintangible/large1.jpg" />
<img style="display:none;" src="asset/img/lintangible/large1_m.jpg" />
<div class="lintangible project-body" style="background-color: #152E5F;">
  <div class="project-first">
    <div class="project-first-inner project-first-desktop" style="background-image: url('asset/img/lintangible/large1.jpg');" border="CBC9C9">
      <img class="floating-arrow" id="arrow_scroll" src="asset/img/arrow_light.svg" />
    </div>
    <div class="project-first-inner project-first-mobile" style="background-image: url('asset/img/lintangible/large1_m.jpg');" border="CBC9C9">
      <img class="floating-arrow" id="arrow_scroll2" src="asset/img/arrow_light.svg" />
    </div>
  </div>
  <div class="project-logo">
    <img src="asset/img/lintangible/logo.svg" class="protect" />
  </div>

  <div class="project-info" id="project_info">
    <div class="project-client project-client-white">
      <h2>Client</h2>
      <hr>
      <p>L’intangible est un restaurant gastronomique créatif tenu par deux jeunes chefs.
        Emma, pâtissière et Antoine, cuisinier y allient leurs savoirs faire pour proposer
        une cuisine inventive et émotive à base de produits locaux et de qualité.
        Situé dans le Tarn au château de la Bousquétarié, ils proposent une expérience authentique et hors du&nbsp;temps.</p>
    </div>
    <div class="project-keywords project-keywords-white">
      <span>05</span>
      <h2>Mots clés</h2>
      <hr>
      <div>
        <p>Identité Visuelle / Direction artistique / Logotype / Papeterie / Typographie</p>
        <strong>Nourrir l’émotion</strong>
      </div>
    </div>
  </div>

  <div class="project-image-full">
    <img src="asset/img/lintangible/large2.jpg" />
  </div>

  <div class="project-two-grid" style="background-color: #EFEDE6;">
    <div class="lintangible-left-small">
      <img src="asset/img/lintangible/small1.jpg" data-aos="fade-right" data-aos-duration="1000" />
    </div>
    <div class="lintangible-right">
      <div>
        <h3>
          Identité Visuelle,<br>
          Direction artistique,<br>
          Design graphic,<br>
          Menus,<br>
          Collaterals<br>
        </h3>
        <i>POUR</i>
        <img src="asset/img/lintangible/emma_antoine.svg" />
        <span>
          Château de la Bousquétarié,<br>
          81700<br>
          Lempaut<br>
        </span>
      </div>
    </div>
  </div>

  <div class="project-two-grid" style="background-color: #283328;">

    <div class="lintangible-left-relative">
      <div class="lintangible-two-grid">
        <img src="asset/img/lintangible/image3.svg" />
        <p>
          Photographie <br>
          Hugo Zely
        </p>
      </div>

      <div class="lintangible-center-relative">
        <img src="asset/img/lintangible/image1.svg" />
        <p>
          Par Emma et Antoine
        </p>
      </div>

    </div>

    <div class="lintangible-right-full">
      <img src="asset/img/lintangible/image2.jpg" />
    </div>


  </div>

  <div class="lintangible-center">
    <h3>Création typographique</h3>
    <img src="asset/img/lintangible/name.svg" />
    <p>Nous avons dessiné une typographie unique pour la création de cette identité visuelle.
      À travers celle-ci nous avons représenté le caractère subtil, fin et original de la cuisine proposé par Emma et Antoine.</p>
    <img src="asset/img/lintangible/monogram_white.svg" />
  </div>

  <div style="background-color: #B1C1E0; padding: 80px 5vw;">
    <div class="project-image-large">
      <img src="asset/img/lintangible/large3.jpg" class="protect" data-aos="fade-up" data-aos-duration="1000" />
    </div>
  </div>

  <div class="project-image-full">
    <img src="asset/img/lintangible/image4.jpg" />
  </div>

  <div class="project-two-grid">
    <div class="lintangible-left lintangible-direction">
      <img src="asset/img/lintangible/menu.svg" />
      <div>
        <h3>Un restaurant créatif</h3>
        <p>Les chefs proposent un menu unique qui puise dans la richesse des saisons et du patrimoine.
          Un menu constitué en fonction des saisons et des produits des producteurs locaux.</p>
      </div>
      <img src="asset/img/lintangible/monogram.svg" />
    </div>
    <div class="lintangible-right-full">
      <img src="asset/img/lintangible/small2.jpg" data-aos="fade-left" data-aos-duration="1000" />
    </div>
  </div>

  <div class="project-two-grid lintangible-box-sizing">
    <div>
      <img src="asset/img/lintangible/image5.jpg" data-aos="fade-left" data-aos-duration="1000" />
    </div>
    <div class="lintangible-image-center">
      <img src="asset/img/lintangible/image6.jpg" data-aos="fade-left" data-aos-duration="1000" />
    </div>
  </div>

  <div class="project-image-full">
    <img src="asset/img/lintangible/large4.jpg" />
  </div>

  <div class="project-two-grid" style="background-color: #283328;">
    <div class="lintangible-right-full">
      <img src="asset/img/lintangible/image7.jpg" />
    </div>

    <div class="lintangible-left-relative2">
      <img src="asset/img/lintangible/image8.svg" />
      <div class="lintangible-two-grid2">
        <p>
          L’art de la cuisine
        </p>
        <p>
          «&nbsp;L’intangible c’est un moment délicieux que le client doit garder en mémoire toute sa vie.
          Nous sommes comme des marchands de bonheur, qui créent une sensation éphémère, mais qui doit aussi laisser une empreinte à vie.&nbsp;»
        </p>
        <p>
          Alain ducasse
        </p>
      </div>
      <img src="asset/img/lintangible/image9.svg" />
      <div class="lintangible-center-grid">
        <p>
          photographie <br>
          hugo zely
        </p>
      </div>
    </div>
  </div>

  <div class="project-two-grid lintangible-gap" style="background-color: #152E5F;">
    <div data-aos="fade-right" data-aos-duration="1000">
      <img src="asset/img/lintangible/small5.jpg" />
    </div>
    <div data-aos="fade-left" data-aos-duration="1000">
      <img src="asset/img/lintangible/small6.jpg" />
    </div>
  </div>

  <div class="project-two-grid">
    <div data-aos="fade-right" data-aos-duration="1000">
      <img src="asset/img/lintangible/small3.jpg" />
    </div>
    <div data-aos="fade-left" data-aos-duration="1000">
      <img src="asset/img/lintangible/small4.jpg" />
    </div>
  </div>

  <div class="lintangible-image-large">
    <img src="asset/img/lintangible/image10.jpg" alt="">
  </div>

  <div class="project-two-grid">
    <div class="lintangible-color">
      <p>DESIGN MONOGRAME</p>
      <img src="asset/img/lintangible/image12.svg" />
      <p>L + i</p>
    </div>
    <div class="lintangible-right-full">
      <img src="asset/img/lintangible/image11.jpg" data-aos="fade-left" data-aos-duration="1000" />
    </div>
  </div>

  <div style="background-color: #EFEDE6; padding-top: var(--margin);" class="lintangible-bottom">
    <img src="asset/img/lintangible/losange.svg" data-aos="fade-up" data-aos-duration="1000" />

    <div class="project-bottom">
      <div class="project-bottom-block">
        <h2>L'Intangible</h2>
      </div>
      <div class="project-bottom-block">
        <h2>Restauration</h2>
      </div>
      <div class="project-bottom-block">
        <h2>2021</h2>
      </div>
    </div>
  </div>

  <div style="background-color: #0f0f0f;">
    <div class="project-bottom-button">
      <a href="projets" class="double-button studio-button-center">
        <div class="double-button-back">
          RETOUR&nbsp;AUX&nbsp;PROJETS
        </div>
        <p class="double-button-text">
          RETOUR&nbsp;AUX&nbsp;PROJETS
        </p>
      </a>
    </div>
  </div>
</div>
<script>
</script>
<script src="asset/js/project.js"></script>
<?php
include "foot.php";
?>