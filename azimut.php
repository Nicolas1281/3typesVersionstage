<?php
$menu = "none";
$title = "Azimut - 3Types";
$bodyback = "#283328";
$project = "azimut";
include "head.php";
?>
<script src="asset/js/shuffle.min.js"></script>
<script src="asset/js/gsap.min.js"></script>
<script src="asset/js/doublebutton.js"></script>

<img style="display:none;" src="asset/img/azimut/image1.jpg" />
<img style="display:none;" src="asset/img/azimut/image1.jpg" />
<div class="project-body" style="background-color: #283328;">
  <div class="project-first">
    <div class="project-first-inner project-first-desktop" style="background-image: url('asset/img/azimut/image1.jpg');" border="151515">
      <img class="floating-arrow" id="arrow_scroll" src="asset/img/arrow_light.svg" />
      <img class="azimut-absolute" src="asset/img/azimut/image2.svg" alt="">
    </div>
    <div class="project-first-inner project-first-mobile" style="background-image: url('asset/img/azimut/image1.jpg');" border="151515">
      <img class="floating-arrow" id="arrow_scroll2" src="asset/img/arrow_light.svg" />
      <img class="azimut-absolute" src="asset/img/azimut/image2.svg" alt="">
    </div>
  </div>

  <div class="project-info" id="project_info">
    <div class="project-client project-client-white">
      <h2>Client</h2>
      <hr>
      <p>
        Azimut est une marque de prêt à porter Française dédiée au voyage.
        Nous avons réalisé l’identité visuelle de la marque, les motifs des vêtements, le suivi de fabrication et d’impression
        ainsi que la création de toute la campagne de communication Ulule.
      </p>
    </div>
    <div class="project-keywords project-keywords-white">
      <span>06</span>
      <h2>Mots clés</h2>
      <hr>
      <div>
        <p>Branding / Logotype / Illustration / Direction artistique / Identité visuelle / Papèterie </p>
        <strong>Marque Vestimentaire</strong>
      </div>
    </div>
  </div>

  <div class="project-two-grid">
    <div class="azimut-left1">
      <p>LA MARQUE QUI VOYAGE</p>
      <div>
        <p>
          AZIMUT
        </p>
        <p>
          Tout au long de l’année nous avons dessiné, suivi la fabrication des vêtements et créé la campagne de communication de la marque. <br>
          Nous voulions créer une marque responsable qui se veut écologique et soucieuse des conditions de fabrication.
          Le lancement de la première collection s’est fait en financement participatif pour limiter les stocks et le gaspillage afin de limiter
          l’impact sur l’environnement.
          Tous les vêtements sont fabriqué dans un beau coton 100% biologique
        </p>
      </div>

      <img src="asset/img/azimut/image3.svg" alt="">
    </div>
    <div class="azimut-right1">
      <img src="asset/img/azimut/image4.jpg" />
    </div>
  </div>


  <div class="project-three-grid">
    <div class="azimut-left2">
      <p>COLLECTION : OROSHI</p>
    </div>
    <div class="azimut-center2">
      <p>FROM TRAVELERS TO TRAVELERS</p>
    </div>
    <div class="azimut-right2">
      <p>2021</p>
    </div>

  </div>

  <div class="azimut-center3">
    <img src="asset/img/azimut/image5.svg" />
    <p>
      Identité Visuelle, Direction artistique, design graphic, menus, collaterals, communication
    </p>
  </div>

  <div class="azimut-image-large1" style="background-color: #4E5D4E; padding-top: var(--margin); padding-bottom: var(--margin);">
    <img src="asset/img/azimut/image6.jpg" alt="">
  </div>

  <div class="project-two-grid" style="background-color: #283328;">
    <div class="azimut-left4">
      <img src="asset/img/azimut/image7.jpg" />
    </div>

    <div class="azimut-right4">
      <div class="azimut-two-grid">
        <img src="asset/img/azimut/image8.svg" />
        <p>
          LE <br>
          RÊVE <br>
          DE TROIS <br>
          AMIS
        </p>
      </div>

      <div class="azimut-texte-center">
        <p>
          L’IDÉE
        </p>
        <p>
          «&nbsp;L’idée est de s’inspirer de cultures découvertes auprès d’autochtones et de lieux visités afin de les retranscrire dans notre marque.
          Grâce à nos voyages, nous voulons nous éloigner des clichés qui peuvent entourer certains pays et créer des collections
          fidèles à ce que nous avons vu et vécu.&nbsp;»
        </p>
        <p>
          LÉA MOLES
        </p>
        <img src="asset/img/azimut/image9.svg" alt="">
      </div>

    </div>
  </div>

  <div class="project-two-grid">
    <div class="azimut-left5">
      <p class="azimut-absolute-left">
        T-SHIRT <br>
        KURAMA
      </p>
      <div class="azimut-absolute2">
        <p>
          Inspiré par le <br>
          temple Kurama
        </p>
        <p>
          Le T-shirt Kurama est unisexe et taille légèrement oversize.
          Il est entièrement en coton bio et a une belle épaisseur qui lui permet de garder sa forme et de durer dans le temps.
        </p>
        <img class="azimut-center5" src=" asset/img/azimut/image10.svg" alt="">
      </div>
    </div>

    <div class="azimut-right5">
      <img src="asset/img/azimut/image11.jpg" />
    </div>
  </div>

  <div class="azimut">
    <img src="asset/img/azimut/image12.jpg" />
  </div>

  <div class="project-two-grid azimut-box-sizing">
    <div>
      <img src="asset/img/azimut/image13.jpg" />
    </div>

    <div>
      <img src="asset/img/azimut/image14.jpg" />
    </div>
  </div>



  <div class="project-two-grid" style="background-color: #4E5D4E;">
    <div class="azimut-left6">
      <img src="asset/img/azimut/image15.jpg" />
    </div>

    <div class="azimut-right6">

      <p class="azimut-absolute-left">
        Hoodie <br>
        Kyoto
      </p>

      <div class="azimut-texte-center1">
        <p>
          Hoodie Kyoto
        </p>
        <p>
          Notre hoodie Kyoto est fabriqué en coton biologique et polyester recyclé.
          Il vous tiendra au chaud même au plus froid de l’hiver. Ce sweat arbore fièrement une grande pagode,
          bâtiment emblématique de la culture japonaise ainsi qu’un motif abstrait sur l’avant.
          Nous espérons qu’il vous plaira autant qu’il est confortable.
        </p>

        <img src="asset/img/azimut/image16.svg" alt="">
      </div>

    </div>
  </div>

  <div class="project-image-large azimut-project-image-large" style="background-color: #283328; padding-top: var(--margin); padding-bottom: var(--margin); position:relative;">
    <img src="asset/img/azimut/image17.jpg" alt="">
    <img src="asset/img/azimut/image18.svg" alt="">
  </div>

  <div class="azimut-two-grid2">
    <div class="azimut-left7">
      <p>
        Kyoto fut la capitale impériale du Japon de 794 à 1868. Elle a hérité de cette époque de nombreux palais,
        des milliers de sanctuaires shinto et de temples bouddhistes, ce qui en fait aujourd’hui le cœur culturel du Japon.
      </p>
    </div>

    <div class="azimut-right7">
      <img src="asset/img/azimut/image19.svg" />
    </div>
  </div>

  <div class="project-two-grid azimut-box-sizing" style="background-color: #E3E2DD;">
    <div>
      <img src="asset/img/azimut/image20.jpg" />
    </div>

    <div>
      <img src="asset/img/azimut/image21.jpg" />
    </div>
  </div>

  <div class="project-two-grid" style="background-color:#4E5D4E;">
    <div class="azimut-right6">

      <p class="azimut-absolute-left">
        Haori <br>
        Hokkaido
      </p>

      <div class="azimut-texte-center1">
        <p>
          Haori Hokkaido
        </p>
        <p>
          Cette veste traditionnelle est directement confectionnée au Japon.
          Nous voulions que cette pièce emblématique de notre collection soit fabriquée dans son pays d’origine,
          dans les règles de l’art, et c’est chose faite ! Avec sa coupe ample et son tissu léger,
          cet haori s’adapte parfaitement à chaque saisons. Par dessus un pull en hiver et au dessus d’un T-shirt en été,
          cette pièce deviendra un incontournable de votre garde-robe.
        </p>

        <img src="asset/img/azimut/image22.svg" alt="">
      </div>

    </div>

    <div class="azimut-left6">
      <img src="asset/img/azimut/image23.jpg" />
    </div>

  </div>

  <div class="project-two-grid azimut-box-sizing" style="background-color: #283328;">
    <div>
      <img src="asset/img/azimut/image24.jpg" />
    </div>

    <div>
      <img src="asset/img/azimut/image25.jpg" />
    </div>
  </div>

  <div class="azimut-two-grid2">
    <div class="azimut-left7">
      <p>
        La grande île du nord du Japon. Elle est recouverte sur près des trois-quarts de son territoire de forêts.
        C’est la destination rêvée des amoureux de nature. Cette île offre une grande variété de paysages,
        allant des denses forêts aux montagnes volcaniques. Randonnée en été, ski en hiver, sans oublier ses nombreux onsens
        (bains d’eau chaude naturels), Hokkaido a de quoi séduire tous les aventuriers du Japon.
      </p>
    </div>

    <div class="azimut-right7">
      <img src="asset/img/azimut/image26.svg" />
    </div>
  </div>

  <div class="azimut-image-large1" style="background-color: #E3E2DD; padding-top: var(--margin); padding-bottom: var(--margin);">
    <img src="asset/img/azimut/image27.jpg" alt="">
  </div>

  <div class="azimut">
    <img src="asset/img/azimut/image28.jpg" />
  </div>

  <div class="azimut-image-large1" style="background-color: #283328; padding-top: var(--margin); padding-bottom: var(--margin);">
    <img src="asset/img/azimut/image29.jpg" alt="">
  </div>

  <div class="project-bottom" style="background-color: #283328;">
    <div class="project-bottom-block">
      <h2>Azimut</h2>
    </div>
    <div class="project-bottom-block">
      <h2>Marque de prêt à porter</h2>
    </div>
    <div class="project-bottom-block">
      <h2>2022</h2>
    </div>
  </div>

  <div class="project-bottom-button" style="background-color: #0F0F0F;">
    <a href="projets" class="double-button studio-button-center">
      <div class="double-button-back">
        RETOUR&nbsp;AUX&nbsp;PROJETS
      </div>
      <p class="double-button-text">
        RETOUR&nbsp;AUX&nbsp;PROJETS
      </p>
    </a>
  </div>
</div>






<script src="asset/js/project.js"></script>
<?php
include "foot.php";
?>