<?php
$menu = "none";
$title = "brothersburger - 3Types";
$bodyback = "#182A65";
$project = "brothersburger";
include "head.php";
?>
<script src="asset/js/shuffle.min.js"></script>
<script src="asset/js/gsap.min.js"></script>
<script src="asset/js/doublebutton.js"></script>

<img style="display:none;" src="asset/img/brothersburger/image1.jpg" />
<img style="display:none;" src="asset/img/brothersburger/image1.jpg" />
<div class="project-body" style="background-color: #182A65;">
    <div class="project-first">
        <div class="project-first-inner project-first-desktop" style="background-image: url('asset/img/brothersburger/image1.jpg');" border="151515">
            <img class="floating-arrow" id="arrow_scroll" src="asset/img/arrow_light.svg" />
            <img class="brothersburger-absolute" src="asset/img/brothersburger/image2.png" alt="">
            <img class="brothersburger-absolute2" src="asset/img/brothersburger/image26.svg" alt="">
        </div>
        <div class="project-first-inner project-first-mobile" style="background-image: url('asset/img/brothersburger/image1.jpg');" border="151515">
            <img class="floating-arrow" id="arrow_scroll2" src="asset/img/arrow_light.svg" />
            <img class="brothersburger-absolute" src="asset/img/brothersburger/image2.png" alt="">
            <img class="brothersburger-absolute2" src="asset/img/brothersburger/image26.svg" alt="">
        </div>
    </div>

    <div class="project-info" id="project_info">
        <div class="project-client project-client-white">
            <h2>Client</h2>
            <hr>
            <p>
                Brothers Burger, c’est le projet de deux frères passionnés de cuisine. Ils créent leurs recettes en respectant les produits,
                les saisons et surtout les producteurs et artisans qu’ils privilégient en culture bio et circuit court.
                Nous avons réalisé leur identité visuelle les différents supports imprimé, leur site web et les photos de tous leurs burgers.
            </p>
        </div>
        <div class="project-keywords project-keywords-white">
            <span>05</span>
            <h2>Mots clés</h2>
            <hr>
            <div>
                <p>Branding / Logotype / Illustration / Direction artistique / Identité visuelle / Papèterie / Web design / Photographie</p>
                <strong>Burger 100% Occitant</strong>
            </div>
        </div>
    </div>

    <div class="project-two-grid">
        <div class="brothersburger-left">
            <img src="asset/img/brothersburger/image4.svg" />
            <div class="brothersburger-text">
                <p>
                    Identité Visuelle, <br>
                    SITE WEB, <br>
                    Direction artistique, <br>
                    design graphic, <br>
                    menus, <br>
                    collaterals
                </p>
                <p>POUR</p>
            </div>
            <img src="asset/img/brothersburger/image3.svg" class="Brothersburger-image" />
            <span>
                Adresse variable <br>
                <a href="https://brothersburger31.fr/" target="_blank">Voir site web</a>
                </a>
            </span>
        </div>
        <div class="brothersburger-right">
            <img src="asset/img/brothersburger/image15.svg" />
        </div>
    </div>

    <div class="project-image-large brothersburger-relative" style="background-color: #F4ECE6; padding-top: var(--margin); padding-bottom: var(--margin);">
        <img src=" asset/img/brothersburger/image5.jpg" data-aos="fade-right" data-aos-duration="500" data-aos-delay="500" style="width: 100%;">
        <img src="asset/img/brothersburger/image6.svg" alt="">
    </div>

    <div class="bands">
        <div class="band1">
            <span>FRAIS, LOCAL, FAIT MAISON, CREATIF, FRAIS, LOCAL, FAIT MAISON, CREATIF, FRAIS, LOCAL, FAIT MAISON, CREATIF, FRAIS, LOCAL, FAIT MAISON, CREATIF,&nbsp;</span>
        </div>
        <div class="band2">
            <span>FRAIS, LOCAL, FAIT MAISON, CREATIF, FRAIS, LOCAL, FAIT MAISON, CREATIF, FRAIS, LOCAL, FAIT MAISON, CREATIF, FRAIS, LOCAL, FAIT MAISON, CREATIF,&nbsp;</span>
        </div>
    </div>

    <div class="project-two-grid">
        <div class="brothersburger-right2">
            <img src="asset/img/brothersburger/image7.png" />
            <img src="asset/img/brothersburger/image14.svg" />
        </div>
        <div class="brothersburger-left2">

            <img src="asset/img/brothersburger/image8.svg" />
            <div class="brothersburger-text-left">
                <p>
                    UN PROJET DE FAMILLE
                </p>
                <p>
                    Deux frères passionnés de cuisine réunis autour d’un projet et l’envie de partager.
                    Nous créons nos recettes en respectant les produits, les saisons et surtout nos producteurs que nous privilégions en culture bio
                    et circuit cours.
                </p>
            </div>
            <img src="asset/img/brothersburger/image9.svg" class="Brothersburger-image2" />
        </div>
    </div>

    <div class="page" id="page">
        <div id="page__body">
            <div class="hero" id="hero">
                <img class="cloud cloud1" src="/asset/img/brothersburger/image10.svg" />
                <img class="cloud cloud2" src="/asset/img/brothersburger/image11.svg" />
                <img class="cloud cloud3" src="/asset/img/brothersburger/image12.svg" />
                <svg class="text-anim" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 218 542" width="218" height="542">
                    <path id="curve_text" fill="none" d="m217 542h-217v-434c0-59.9 48.1-108 107.4-108 59.3 0 110.6 48 110.6 111z" />
                    <text>
                        <textPath xlink:href="#curve_text" startOffset="1%">
                            <tspan>FAIT MAISON AVEC DES PRODUITS LOCAUX</tspan>
                            <tspan>&nbsp;&nbsp;✹&nbsp;&nbsp;</tspan>
                            <tspan>FAIT MAISON AVEC DES PRODUITS LOCAUX</tspan>
                            <tspan>&nbsp;&nbsp;✹&nbsp;&nbsp;</tspan>
                            <tspan>FAIT MAISON AVEC DES PRODUITS LOCAUX</tspan>
                            <tspan>&nbsp;&nbsp;✹&nbsp;&nbsp;</tspan>
                            <animate attributeName="startOffset" from="-20%" to="18.1%" begin="0s" dur="12s" repeatCount="indefinite" />
                        </textPath>
                    </text>
                </svg>
                <div class="hero-img">
                    <img id="hero_img" src="/asset/img/brothersburger/image13.jpg" />
                </div>
                <div class="arrow cursor-trigger" onclick="jumpTo('who');">
                    <svg xmlns="http://www.w3.org/2000/svg" width="78.211" height="151.095" viewBox="0 0 78.211 151.095">
                        <path d="M1959.442-3.637V-113.756l-16.651,15-16.651-15V-3.637h-18.833l35.484,35.484,35.484-35.484Z" transform="translate(-1903.685 117.127)" fill="#eb61f7" stroke="#eb61f7" stroke-miterlimit="10" stroke-width="3" />
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="78.211" height="151.095" viewBox="0 0 78.211 151.095">
                        <path d="M1959.442-3.637V-113.756l-16.651,15-16.651-15V-3.637h-18.833l35.484,35.484,35.484-35.484Z" transform="translate(-1903.685 117.127)" fill="#112889" stroke="#eb61f7" stroke-miterlimit="10" stroke-width="3" />
                    </svg>
                    <span>SCROLL</span>
                </div>
            </div>
        </div>
    </div>

    <div class="project-image-large brothersburger-video">
        <p>
            WEB DESIGN
        </p>
        <video autoplay muted loop playsinline width="100%">
            <source src="asset/video/brothersburger/brothersburger.webm" type="video/webm" />
            <source src="asset/video/brothersburger/brothersburger.mp4" type="video/mp4" />
            <source src="asset/video/brothersburger/brothersburger.mov" type="video/mov" />
        </video>
    </div>

    <div class="project-two-grid">
        <div class="brothersburger-left2 brothersburger-color">
            <img src="asset/img/brothersburger/image16.svg" />
            <div class="brothersburger-left-color">
                <p>
                    LOCAL ET FAIT MAISON
                </p>
                <p>
                    Notre pain brioché gourmand est confectionné par notre Artisan Boulanger et livré chaque matin.
                    Nos viandes sont 100% françaises et nos desserts sont 100% faits maison. Nous nous voulons également éco-responsables,
                    nos emballages sont entièrement recyclables et nos serviettes sont biodégradables.
                </p>
            </div>
            <img src="asset/img/brothersburger/image17.svg" class="Brothersburger-image2" />
        </div>
        <div class="brothersburger-right3">
            <img src="asset/img/brothersburger/image18.jpg" />
        </div>

    </div>

    <div class="project-image-large brothersburger-image-absolute" style="background-color: #F4ECE6; padding-top: var(--margin); padding-bottom: var(--margin);">
        <img src=" asset/img/brothersburger/image19.jpg" data-aos="fade-right" data-aos-duration="500" data-aos-delay="500" style="width: 100%;">
        <img src="asset/img/brothersburger/image20.svg" alt="">
        <img src="asset/img/brothersburger/image21.svg" alt="">
        <img src="asset/img/brothersburger/image22.svg" alt="">
        <img src="asset/img/brothersburger/image23.svg" alt="">
    </div>

    <div class="brothersburger-watching">
        <img src=" asset/img/brothersburger/image24.svg">
        <p>Thanks for watching !</p>
    </div>

    <div class="project-bottom">
        <div class="project-bottom-block">
            <h2>Brothers Burger</h2>
        </div>
        <div class="project-bottom-block">
            <h2>Burger maison réalisé avec des produits locaux</h2>
        </div>
        <div class="project-bottom-block">
            <h2>2022</h2>
        </div>
    </div>

    <div class="project-bottom-button" style="background-color: #0F0F0F;">
        <a href="projets" class="double-button studio-button-center">
            <div class="double-button-back">
                RETOUR&nbsp;AUX&nbsp;PROJETS
            </div>
            <p class="double-button-text">
                RETOUR&nbsp;AUX&nbsp;PROJETS
            </p>
        </a>
    </div>
</div>

<script src="asset/js/project.js"></script>
<?php
include "foot.php";
?>