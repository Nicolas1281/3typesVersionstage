<?php
$menu = "none";
$title = "ubf - 3Types";
$bodyback = "#555CAC";
$project = "ubf";
include "head.php";
?>
<script src="asset/js/shuffle.min.js"></script>
<script src="asset/js/gsap.min.js"></script>
<script src="asset/js/doublebutton.js"></script>

<img style="display:none;" src="asset/img/ubf/image1.jpg" />
<img style="display:none;" src="asset/img/ubf/image1.jpg" />
<div class="project-body" style="background-color: #555CAC;">
    <div class="project-first">
        <div class="project-first-inner project-first-desktop" style="background-image: url('asset/img/ubf/image1.jpg');" border="151515">
            <img class="floating-arrow" id="arrow_scroll" src="asset/img/arrow_light.svg" />
            <img class="ubf-absolute" src="asset/img/ubf/image4.svg" alt="">
        </div>
        <div class="project-first-inner project-first-mobile" style="background-image: url('asset/img/ubf/image1.jpg');" border="151515">
            <img class="floating-arrow" id="arrow_scroll2" src="asset/img/arrow_light.svg" />
            <img class="ubf-absolute" src="asset/img/ubf/image4.svg" alt="">
        </div>
    </div>

    <div class="project-info" id="project_info">
        <div class="project-client project-client-white">
            <h2>Client</h2>
            <hr>
            <p>
                ULTRA BIKE FRANCE organise des compétition d’ultra bike (course de 500 km) dans tous les départements français.
                Nous avons réalisé avec eux leur site internet qui présente les courses et permet de réserver sa place place en ligne.
            </p>
        </div>
        <div class="project-keywords project-keywords-white">
            <span>05</span>
            <h2>Mots clés</h2>
            <hr>
            <div>
                <p>Design web / web development</p>
                <strong>COMPÉTITION DE VÉLO</strong>
            </div>
        </div>
    </div>

    <div class="project-two-grid">
        <div class="ubf-left">
            <img src="asset/img/ubf/image3.svg" />
            <p>
                Web design <br>
                Website Development
            </p>
            <span>
                Web design / Website development : 3Types <br>
                Graphisme : Quatrième Étage <br>
                Toulouses
                </a>
            </span>
        </div>
        <div class="ubf-right">
            <img src="asset/img/ubf/image2.jpg" />
            <video autoplay muted loop playsinline width="100%">
                <source src="asset/video/ubf/ubf_m.webm" type="video/webm" />
                <source src="asset/video/ubf/ubf_m.mp4" type="video/mp4" />
                <source src="asset/video/ubf/ubf_m.mov" type="video/mov" />
            </video>
        </div>
    </div>

    <div class="project-image-large ubf-video">
        <video autoplay muted loop playsinline width="100%">
            <source src="asset/video/ubf/ubf.webm" type="video/webm" />
            <source src="asset/video/ubf/ubf.mp4" type="video/mp4" />
            <source src="asset/video/ubf/ubf.mov" type="video/mov" />
        </video>
    </div>

    <div class="project-bottom">
        <div class="project-bottom-block">
            <h2>Ultra Bike France</h2>
        </div>
        <div class="project-bottom-block">
            <h2>Course cycliste</h2>
        </div>
        <div class="project-bottom-block">
            <h2>2022</h2>
        </div>
    </div>

    <div class="project-bottom-button" style="background-color: #0F0F0F;">
        <a href="projets" class="double-button studio-button-center">
            <div class="double-button-back">
                RETOUR&nbsp;AUX&nbsp;PROJETS
            </div>
            <p class="double-button-text">
                RETOUR&nbsp;AUX&nbsp;PROJETS
            </p>
        </a>
    </div>
</div>

<script src="asset/js/project.js"></script>
<?php
include "foot.php";
?>